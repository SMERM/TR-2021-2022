# TR-2021-2022

Ciclo di Triennio: classe di studenti entrati nell'A.A. 2021-2022

# Classe

* Babak Davami
* ~Ilaria Gunn~ (+)
* Francesco Vesprini
* ~Valerio Timo~ (++)

(+) ha lasciato il corso durante l'A.A. 2021-2022
(++) ha lasciato il corso durante l'A.A. 2022-2023

# LICENZA

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">TR-2021-2022</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://git.smerm.org/SMERM/TR-2021-2022" rel="dct:source">https://gitlab.com/SMERM/TR-2021-2022</a>
[Full text license](./LICENSE)
