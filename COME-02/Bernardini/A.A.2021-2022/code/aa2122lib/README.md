# Libreria `aa2122lib`

## [math](./math.py)

Classi di funzioni matematiche

## [processo](./processo.py)

Classi base di processi sonori

## [suono](./suono.py)

Classi di suoni
