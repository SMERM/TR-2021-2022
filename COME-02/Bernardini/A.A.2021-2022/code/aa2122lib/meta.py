import importlib
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def dynamic_importer(modname, class_list):
    """
        dynamic_importer(nome_del_modulo, lista_di_classi_o_nomi_da_importare)

        importa un modulo dinamicamente e ritorna le definizioni di classe richieste
    """
    mod = __import__(modname, fromlist=class_list)
    result = []
    for cname in class_list:
        result.append(getattr(mod, cname))
    return result

def instantiate_class(klass, dict_data, properties):
    result = klass()
    dict_data.update(properties)
    result.__dict__ = dict_data
    return result

def load_single_metadatum(datum, key):
    result = None
    if type(datum) is dict:
        properties = {}
        for (key, item) in datum.items():
            if type(item) is dict:
                properties[key] = load_single_metadatum(item, key)
            elif type(item) is list:
                properties[key] = []
                for n in item:
                    properties[key].append(load_single_metadatum(n, key))
        if 'class' in datum:
            module_name = datum['class'].lower()
            if 'module_name' in datum:
                module_name = datum['module_name']
            klass = dynamic_importer(module_name, [datum['class']])[0]
            result = instantiate_class(klass, datum, properties)
    else:
        raise StandardError("YAML datum \"%s\" is not a dictionary")
    return result

def load_metadata(yaml_file):
    fh = open(yaml_file, "r")
    md = load(fh, Loader=Loader)
    fh.close()
    result = []
    for (key, item) in md.items():
        result.append(load_single_metadatum(item, key))

    return result
