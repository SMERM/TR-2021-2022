import sys
sys.path.append('../../..')

from aa2122lib.processo import Processo

class SubMetaClass:

    def inspect(self):
        return "; classe %s - proprieta_b: %d" % (self.__class__.__name__, self.proprieta_b)

class MetaClass(Processo):

    def __run__(self):
        result = "classe %s - proprieta_a: %d" % (self.__class__.__name__, self.proprieta_a)
        result += self.sotto_classe.inspect()
        return result
