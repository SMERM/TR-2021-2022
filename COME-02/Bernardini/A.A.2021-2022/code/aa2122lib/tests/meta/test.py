#
# Test di una classe meta-generata
#
import pdb
import sys
import unittest
sys.path.append('../../..')
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from aa2122lib.meta import load_metadata
from metaclass import MetaClass, SubMetaClass

class TestClass(unittest.TestCase):

    def setUp(self):
        self.loaded_classes = load_metadata('test.meta')
        fh = open('test.meta', 'r')
        self.meta_data = load(fh, Loader=Loader)
        fh.close()

    def test_loading(self):
        [self.assertIsInstance(lk, lk.__class__) for lk in self.loaded_classes]

    def test_load_the_proper_classes(self):
        [self.assertEqual(lk.__class__.__name__, 'MetaClass') for lk in self.loaded_classes]
        for lk in self.loaded_classes:
            self.assertEqual(lk.sotto_classe.__class__.__name__, 'SubMetaClass')
            self.assertEqual(len(lk.array_di_sotto_classi), 2)
            for slk in lk.array_di_sotto_classi:
                self.assertEqual(slk.__class__.__name__, 'SubMetaClass')

    def test_derivation(self):
        for lk in self.loaded_classes:
            result = lk.run()  # works only if tested class is a subprocess of Processo
            self.assertTrue(type(result) is str)

    def test_proper_instantiantion(self):
        idx = 0
        for k in self.loaded_classes:
            key = "t%d" % (idx)
            self.assertEqual(k.proprieta_a, self.meta_data[key]['proprieta_a'])
            self.assertEqual(k.sotto_classe.proprieta_b, self.meta_data[key]['sotto_classe']['proprieta_b'])
            idx2 = 0
            for kk in k.array_di_sotto_classi:
                self.assertEqual(kk.proprieta_b, self.meta_data[key]['array_di_sotto_classi'][idx2]['proprieta_b'])
                idx2 += 1
            idx += 1

if __name__ == '__main__':
    unittest.main()
