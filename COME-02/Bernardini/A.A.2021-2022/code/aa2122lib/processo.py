import sys

class Processo:
    """
        Processo: classe base di tutti i processi.
        Utile per derivare facilmente processi.
    """

    def __run__(self):
        """ questo e` il metodo da derivare """
        return ""

    def run(self):
        result = self.header()
        result += self.__run__()
        result += self.trailer()
        return result

    def header(self):
        return ";\n; Inizio processo %s\n;\n" % (self.__class__.__name__)

    def trailer(self):
        return ";\n; Fine processo %s\n;\n" % (self.__class__.__name__)

    def print(self, output=sys.stdout):
        print(self.run(), file=output)
