# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
# Prima annualità (A.A.2021-2022)

## Macro-esercitazioni (Milestones)

### Attribuzione del punteggio degli esercizi

| Caratteristica            | Punteggio |
|---------------------------|-----------|
| Musicalità d'insieme      | + 2 punti |
| Realizzazione tecnica     | + 1 punto |
| Qualità sonora            | + 1 punto |
| Puntualità della consegna | + 1 punto |

### Esercizio 1: collage concreto (algoritmico) (+ 5 punti)

* solo frammenti concreti
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori
* durata: 3-10 minuti
* data di consegna: entro domenica 16 gennaio 2022 ore 13:00
* brano di riferimento: [Alain Savouret, *Don Quichotte Corporation*](https://fresques.ina.fr/artsonores/fiche-media/InaGrm00029/alain-savouret-don-quichotte-corporation.html)

### Esercizio 2: sinusoidi semplici con maschere di tendenza (+ 5 punti)

* solo onde periodiche semplici (non modulate - solo inviluppi trapezoidali)
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 2-5 minuti
* data di consegna: entro domenica 7 febbraio 2022 ore 13:00
* brano di riferimento: [Fausto Razzi, *Progetto II*](https://open.spotify.com/track/61LcHYVkacI16YhUjXAivk)

### Esercizio 3: sinusoidi semplici modulate con maschere di tendenza (+ 5 punti) 

* solo onde periodiche semplici con modulazioni a piacere
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 3-6 minuti
* data di consegna: entro domenica 27 febbraio 2022 ore 13:00
* brano di riferimento: [Gyorgy Ligeti, *Artikulation*](https://youtu.be/71hNl_skTZQ)

### Esercizio 4: somme di sinusoidi modulate con maschere di tendenza (+ 5 punti)

* somme di sinusoidi con modulazioni a piacere
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 3-10 minuti
* data di consegna: entro domenica 27 marzo 2022 ore 13:00
* brano di riferimento: [Karlheinz Stockhausen, *Studie II*](https://youtu.be/_qi4hgT_d0o)

### Esercizio 5: combinazioni di somme di sinusoidi modulate con maschere di tendenza e suoni concreti (+ 5 punti)

* somme di sinusoidi con modulazioni a piacere
* suoni concreti
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 5-10 minuti
* data di consegna: entro domenica 8 maggio 2022 ore 13:00
* brano di riferimento: [Barry Truax, *Riverrun*](https://youtu.be/u81IGEFt7dM)

### Esercizio 6: brano d'esame  (+ 5 punti)

* lavoro acusmatico
* vietato utilizzare audio editors
* durata: 5-10 minuti
* data di consegna: data dell'appello d'esame

# Diario di Bordo

| Studente           | Es.1 | Es.2 | Es.3 | Es.4 | Es.5 | Esame | Voto |
|--------------------|:----:|:----:|:----:|:----:|:----:|:-----:|:----:|
| Babak Davami       |  3   |  4   |  5   |  4   |  4   |   5   |  26  |
| Ilaria Gunn        |  3   |  2   |      |      |      |       |      |
| Francesco Vesprini |  4   |  5   |  5   |  5   |  5   |   5   |  29  |
| Valerio Timo       |  4   |  4   |  3   |  3   |  4   |   5   |  25  |
