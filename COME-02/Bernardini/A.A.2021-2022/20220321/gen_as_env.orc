sr=48000
ksmps=6
nchnls=1
0dbfs=1

zakinit 100,100

      instr 1
idur  = p3
iamp  = ampdbfs(p4)
ifun  = p5
ichannel = p6
ioffset = 0.001

kenv  oscil1i 0, iamp, idur, ifun
kenv  = kenv-ioffset

      zkw kenv, ichannel

      endin

      instr 10
iamp  = ampdbfs(p4)
ifreq = cpspch(p5)
ifun  = p6
idur  = p3
ienvfun = p7
ienvoff = p8
idelay  = p9
ichannel = p10

kglobenv zkr ichannel

kamp  oscil1i idelay, iamp, idur, ienvfun
kamp  = kamp - ienvoff
aout  oscil kamp, ifreq, ifun
      out   aout*kglobenv

      endin
