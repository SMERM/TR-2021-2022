# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 08/11/2021

### Argomenti

* Introduzione ai 6 esercizi da svolgere durante l'anno
* Ripasso della strumentazione logistica:
  * [calendario SME::Roma](https://calendar.google.com/calendar/u/0?cid=dG41bGs2NmQzcjdsazViaW1ucWxzb2Q1djBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
  * [`git`](https://gitlab.com/SMERM/TR-2020-2021)
  * [`slack`](https://smerm.slack.com) (dettaglio dei canali)
  * [`jitsi`](https://meet.smerm.org/bernardini)
  * [mailing list](https://groups.google.com/g/smerm-studenti)
  * [canale YouTube dedicato](https://www.youtube.com/channel/UCG7pgN812PK0nrAJZQ5FeVA)
* Chiarimenti sulle *funzioni* (vs. i *generi*) della musica
* Problematiche legate all'ascolto odierno: allenamento dell'orecchio
* Problematiche della Composizione Musicale Elettroacustica
  * collocazione nel contesto storico
    * la composizione dopo Cage
    * linguaggio puramente connotativo
  * motivazioni delle scelte tecnologiche
  * l'attenzione per il timbro
  * una concezione olistica della composizione
  * comporre con spazi continui
  * astrazione compositiva, suggestioni narrative, sonificazioni, ecc.
  * che cos'è la *musicalità*? Termodinamica dell'informazione musicale
* scelta delle tecnologie
  * riconsiderare il ruolo del computer (non più *appliance domestica* ma strumento di lavoro professionale)
  * funzionalità delle tecnologie
  * elementi necessari delle tecnologie 
  * strumenti di sintesi
    * [`csound`](https://csounds.com)
    * [`pure data`](http://pure-data.info)
    * [`SuperCollider`](https://supercollider.github.io/)
  * strumenti vari
    * [emulatore di terminale (`bash`)](https://www.gnu.org/software/bash/)
    * [editor di testo *puro ascii* (`atom`, tra i tanti)](https://atom.io/)
    * [editor di suoni - `audacity`](https://www.audacityteam.org/)
    * [analisi numerica - `octave`](https://www.gnu.org/software/octave/index)
    * [tipografia musicale - `lilypond`](https://lilypond.org/) con il suo *front-end* [`frescobaldi`](https://www.frescobaldi.org/index.html)
    * [tipografia musicale - `pic`](https://it.wikipedia.org/wiki/Groff_(software))
  * linguaggi di scripting
    * [`python`](https://python.org)
    * [`ruby`](https://www.ruby-lang.org/it/)

### Studio a casa

* allenamento dell'orecchio: una/due ore quotidiane di ascolto di [RadioTre Classica](https://www.raiplayradio.it/radioclassica/) decidendo quando e come distribuirle nella giornata ma **SENZA SCEGLIERE** in base al contenuto.
* scaricare, installare e verificare il funzionamento di tutti gli strumenti software sopra indicati
* lettura della documentazione online degli strumenti di sintesi sopra esposti
* abbozzo del progetto dell'esercizo 1
