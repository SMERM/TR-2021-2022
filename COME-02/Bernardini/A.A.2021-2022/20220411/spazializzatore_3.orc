;
; spazializzatore 2: diretto e prime riflessioni
;
sr = 48000
ksmps = 64
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

;
; generatori
;
instr 1, 2, 3, 4
  iindex = p1
  ifreq  = p4
	aout gbuzz 1, ifreq, 10000, 1, 1, 2
	
	zawm aout, iindex
endin

;
; strumento che genera la stanza
;
instr 11, 12, 13, 14
	iindex = p1 -10
	ix = p4
	iy = p5
	aout zar iindex
  ;
  ; calcolo del suono diretto
  ;
	;
	; posizione degli altoparlanti
	;
	ilsx table 0, 1
	ilsy table 1, 1
	ilrx table 2, 1
	ilry table 3, 1
	print ilsx, ilsy, ilrx, ilry
  ;
	; calcolare la distanza del suono dagli altoparlanti
	;
	idl = sqrt(((ix - ilsx)^2) + ((iy - ilsy)^2))
	idr = sqrt(((ix - ilrx)^2) + ((iy - ilry)^2))
	print idl, idr
	iampl = giassorb / idl
	iampr = giassorb / idr
	idel = idl / gispeed
	ider = idr / gispeed
	print iampl, iampr, idel, ider
  ;
  ; calcolo delle riflessioni
  ; ;
  ; geometria della stanza
	;
  iwcornerlx  table 4, 1   ; angolo superiore sinistro (x)
  iwcornerly  table 5, 1   ; angolo superiore sinistro (y)
  iwcornerrx  table 6, 1   ; angolo inferiore destro (x)
  iwcornerry  table 6, 1   ; angolo inferiore destro (y)
  ;
  ; posizione delle riflessioni
  ;
  ilrefposxls = ((iwcornerlx-ilsx)*2)+ix ; muro sinistro
  ilrefposxlr = ((iwcornerlx-ilrx)*2)+ix ; muro sinistro
  ilrefposy   = iy
  irrefposxrs = ((iwcornerrx-ilsx)*2)+ix ; muro destro
  irrefposxrr = ((iwcornerrx-ilrx)*2)+ix ; muro destro
  irrefposy = iy
  ifrefposx = ix                ; muro frontale
  ifrefposyfs = ((iwcornerly-ilsy)*2)+iy
  ifrefposyfr = ((iwcornerly-ilry)*2)+iy
  ibrefposx   = ix                ; muro posteriore
  ibrefposybs = ((iwcornerry-ilsy)*2)+iy
  ibrefposybr = ((iwcornerry-ilry)*2)+iy
  print ilrefposxls, ilrefposxlr, ilrefposy, irrefposxrs, irrefposxrr, ifrefposx, ifrefposyfs, ifrefposyfr, ibrefposybs, ibrefposybr
  ;
  ; TODO: 1) calcolare tempi di ritardo e abbattimento di ampiezza
  ;          per tutte le riflessioni
  ;
  ; distanze delle riflessioni
  ; 
  idrefll = sqrt((ilrefposxls^2) + (ilrefposy^2))
  idrefrl = sqrt((ilrefposxlr^2) + (ilrefposy^2))
  idrefrl = sqrt((irrefposxrs^2) + (irrefposy^2))
  idrefrr = sqrt((irrefposxrs^2) + (irrefposy^2))
  idreffl = sqrt((ifrefposx^2)   + (ifrefposyfs^2))
  idreffr = sqrt((ifrefposx^2)   + (ifrefposyfr^2))
  idrefbl = sqrt((ibrefposx^2)   + (ibrefposybs^2))
  idrefbr = sqrt((ibrefposx^2)   + (ibrefposybr^2))
;   ;
;   ; ritardi delle riflessioni
;   ;
;   idrefdell = idrefll / gispeed
;   idrefderl = idrefrl / gispeed
;   idrefdelr = idrefr / gispeed
;   idrefdelf = idreff / gispeed
;   idrefdelb = idrefb / gispeed
;   ;
;   ; abbattimento ampiezze delle riflessioni
;   ;
;   iarefampl = giassorb / idrefl
;   iarefampr = giassorb / idrefr
;   iarefampf = giassorb / idreff
;   iarefampb = giassorb / idrefb

  ; 
  ;       2) calcolare il all pass (per fare il resto)
  ;


  ;
  ;
  ;
  ; TODO:
  ; - fare le riflessioni secondo l'altoparlante sinistro
  ; - fare le riflessioni secondo l'altoparlante destro

; 	abufl delayr 2
;   ;
;   ; diretto
;   ;
; 	adirl deltap idel
; 	adirr deltap ider
;   ;
;   ; riflessioni
;   ;
;   arefl deltap idrefdell
;   arefr deltap idrefdelr
;   areff deltap idrefdelf
;   arefb deltap idrefdelb
;   
; 	delayw aout
; 
;   aleft = adirl*iampl, adirr*iampr, arefl*iarefampl, arefr*iarefampr, areff*iarefampf, arefb*iarefampb
;   aright = 
; 
; 	outs aleft, aright
	
	zacl iindex, iindex
endin
