# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 03/03/2022

### Argomenti

#### Discussione di un problema degli inviluppi di `csound`

![errore `linen`](./errore_linen.png)

#### Introduzione alla collocazione spaziale dei suoni

![spazio 0](./sintesi_spazio_0.png)

#### Implementazione: collocazione spaziale dei suoni, versione 0

![spazio 1](./sintesi_spazio_1.png)

Implementazione in `csound`:

[L'orchestra](./spazializzatore_0.orc)

```csound
sr = 48000
ksmps = 64
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

instr 1, 2, 3, 4
iindex = p1
;qui fare la sintesi
zawm aout, iindex
endin

instr 11, 12, 13, 14
iindex = p1 -10
ix = p4
iy = p5
aout zar iindex
ilsx table 1, 1
ilsy table 2, 1
ilrx table 3, 1
ilry table 4, 1
idl = sqrt(((ix - ilsx)**2) + ((iy - ilsy)**2))
idr = sqrt(((ix - ilrx)**2) + ((iy - ilry)**2))
iampl = giassorb / idl
iampr = giassorb / idr
idel = idl / gispeed
ider = idr / gispeed
abufl delayr 2
adirl deltap idel
adirr deltap ider
delayw aout
outs adirl*iampl, adirr*iampr
endin
```
[La partitura](./spazializzatore_0.sco)

```csound
;
; definizione della geometria della stanza reale (altoparlanti) (quadrato di 2x2 m)
; e della stanza virtuale (quadrato di 20x20 m)
;
f1 0 16 -2 -1 1 1 1 -10 10 10 -10
```
