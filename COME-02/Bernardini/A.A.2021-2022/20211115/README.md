# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 15/11/2021

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali

#### Esercizio 1: Strumentario

##### `csound`

* Orchestra
  * `soundin`
  * `diskin`
  * `linen`
  * `out`
* Partitura
  * p-campi passanti: campione, ampiezza, att/dec
  * sorting delle note in partitura

###### Esempio di `orchestra`

[diskin_driver.orc](./code/diskin_driver.orc)
```csound
;
; diskin driver per l'esercizio 1
;
; da modificare liberamente!
sr = 44100
ksmps=5
nchnls=1

          instr 1

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          out     aout

          endin
```

###### v0: realizzazione manuale

[v0.sco - File realizzato manualmente](./code/v0.sco)

```csound
;
; esempio di partitura manuale
;
; il suono 1 è 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
; il suono 2 è 503504__kirmm__church-choir.wav                 (43.374 secondi)
;
; presi da https://freesound.org

i1 0 1 -8 1 10.5
i1 0.5 0.75 -4 2 4.5 
i1 1.5 0.25 -8 1 4.5
i1 1.5 0.75 -2 2 15.5
```

###### v1: realizzazione casuale (in `python`)

[v1.py](./code/v1.py)
```python
#
# versione 1: segmenti completamente casuali (casualita` controllata)
# un solo file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#
from random import random

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
file   = 1  # soundin.1
fdur   = 22.831 # durata del file

while(now < totdur):
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now, dur, amp, file, skip))
    now += dur
```

###### v2: realizzazione casuale (in `python`) - due campioni

[v2.py](./code/v2.py)
```python
#
# versione 2: segmenti completamente casuali (casualita` controllata)
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
files  = [1, 2]  # soundin.1
fdurs  = [22.831, 43.374] # durata del file

while(now < totdur):
    fileidx = randint(0, 1)
    file = files[fileidx]
    fdur = fdurs[fileidx]
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now, dur, amp, file, skip))
    now += dur
```

###### v2: realizzazione casuale (in `python`) - due campioni - versione object-oriented

[v2-oop.py](./code/v2-oop.py)
```python
#
# versione 2: segmenti completamente casuali (casualita` controllata)
#             versione (object oriented)
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]

while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now, dur, amp, file, skip))
    now += dur
```

###### v3: realizzazione casuale (in `python`) - da polvere a suono

[v3.py](./code/v3.py)
```python
#
# versione 3: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b


totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
halflw = leeway/2
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.3    # percentuale di casualita`

while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur_nominale = linear(now, startdur, 0, enddur, totdur)
    min_dur = dur_nominale * (1-durrng)
    max_dur = dur_nominale * (1+durrng)
    rng_dur = max_dur-min_dur
    dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now-halflw, dur+leeway, amp, file, skip))
    now += dur
```

###### v4: realizzazione casuale (in `python`) - da polvere a suono per segmenti consequenziali

[v4.py](./code/v4.py)
```python
#
# versione 4: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono per segmenti consequenziali
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

    def location(self, value):
        return (value % self.dur)

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b


totdur = 60 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
halflw = leeway/2
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.1    # percentuale di casualita` (10%)
skiprng  = 0.01   # percentuale di randomness

while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur_nominale = linear(now, startdur, 0, enddur, totdur)
    min_dur = dur_nominale * (1-durrng)
    max_dur = dur_nominale * (1+durrng)
    rng_dur = max_dur-min_dur
    dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    ourskip = now+(random()*skiprng*2-skiprng) # collocazione corrente...
    skip = suono.location(ourskip)  # ...modulo la durata del file
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now-halflw, dur+leeway, amp, file, skip))
    now += dur
```

###### v5: segmenti ritmici (in `python`)

[v5.py](./code/v5.py)
```python
#
# versione 5: segmenti ritmici
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

    def location(self, value):
        return (value % self.dur)

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b

def dur_figura(metro, seq, idx):
    ridx = idx % len(seq)
    pulse = 60.0/float(metro)
    return (4*seq[ridx]*pulse)

ritmo = [1/24.0,1/24.0,1/24.0,1/24.0,1/24.0,1/24.0,1/4.0,1/4.0]


totdur = 60 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
halflw = leeway/2
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.1    # percentuale di casualita` (10%)
skiprng  = 0.01   # percentuale di randomness
metro    = 136

idx = 0
while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur = dur_figura(metro, ritmo, idx)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    ourskip = now+(random()*skiprng*2-skiprng) # collocazione corrente...
    skip = suono.location(ourskip)  # ...modulo la durata del file
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now-halflw, dur+leeway, amp, file, skip))
    now += dur
    idx += 1
```

### Lavagna usata durante la lezione

![whiteboard](./whiteboard_20211115.png)

### Studio a casa

* realizzazione pratica dei progetti riguardanti l'esercizio 1
