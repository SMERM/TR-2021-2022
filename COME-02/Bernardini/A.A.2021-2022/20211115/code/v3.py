#
# versione 3: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b


totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
halflw = leeway/2
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.3    # percentuale di casualita`

while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur_nominale = linear(now, startdur, 0, enddur, totdur)
    min_dur = dur_nominale * (1-durrng)
    max_dur = dur_nominale * (1+durrng)
    rng_dur = max_dur-min_dur
    dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now-halflw, dur+leeway, amp, file, skip))
    now += dur
