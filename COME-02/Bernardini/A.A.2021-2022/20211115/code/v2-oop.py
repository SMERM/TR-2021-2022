#
# versione 2: segmenti completamente casuali (casualita` controllata)
#             versione (object oriented)
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
from random import random, randint

class Suono:
    def __init__(self, num, dur):
       self.number = num
       self.dur = dur

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
files  = [ Suono(1, 22.831), Suono(2, 43.374) ]

while(now < totdur):
    fileidx = randint(0, 1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now, dur, amp, file, skip))
    now += dur
