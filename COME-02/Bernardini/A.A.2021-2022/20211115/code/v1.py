#
# versione 1: segmenti completamente casuali (casualita` controllata)
# un solo file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#
from random import random

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
file   = 1  # soundin.1
fdur   = 22.831 # durata del file

while(now < totdur):
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i1 %8.4f %8.4f %+6.2f %1d %8.4f" % (now, dur, amp, file, skip))
    now += dur
