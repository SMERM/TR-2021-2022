;
; diskin driver per l'esercizio 1
;
; da modificare liberamente!
sr = 44100
ksmps=5
nchnls=1

          instr 1

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          out     aout

          endin
