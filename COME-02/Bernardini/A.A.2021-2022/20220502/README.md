# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 02/05/2022

### [Youtube Video](https://youtu.be/Iie0bd8Yt5k)

### Argomenti

* Discussione dei progetti individuali
* Discussione di problemi riguardanti i canali d'uscita di `diskin` (`csound`)
* Continuazione dell'implementazione di un sistema semplice di localizzazione spaziale (`csound`)

#### [Discussione di problemi riguardanti i canali d'uscita di `diskin` (`csound`)](test_diskin.orc)

* pseudo-orchestra:

```csound
sr=48000
ksmps=12
nchnls=1


  instr 1
  ;
  ; lettura di file stereo
  ;
ifile=p4

aleft, aright diskin2 ifile, 1

  out aleft+aright
  endin

  instr 2
  ;
  ; lettura di file mono
  ;
ifile=p4

amono         diskin2 ifile, 1

  out amono
  endin
```

#### [Continuazione dell'implementazione di un sistema semplice di localizzazione spaziale (`csound`)](spazializzatore_4.orc)

* [orchestra](spazializzatore_4.orc):

```csound
;
; spazializzatore 2: diretto e prime riflessioni
;
sr = 48000
ksmps = 2
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

;
; generatori
;
instr 1, 2, 3, 4
  iindex = p1
  ifreq  = p4
	aout mpulse 1, 1
	
	zawm aout, iindex
endin

;
; strumento che genera la stanza
;
instr 11, 12, 13, 14
	iindex = p1 -10
	ix = p4
	iy = p5
	aout zar iindex
  ;
  ; calcolo del suono diretto
  ;
	;
	; posizione degli altoparlanti
	;
	ilsx table 0, 1
	ilsy table 1, 1
	ilrx table 2, 1
	ilry table 3, 1
	print ilsx, ilsy, ilrx, ilry
  ;
	; calcolare la distanza del suono dagli altoparlanti
	;
	idl = sqrt(((ix - ilsx)^2) + ((iy - ilsy)^2))
	idr = sqrt(((ix - ilrx)^2) + ((iy - ilry)^2))
	print idl, idr
	iampl = giassorb / idl
	iampr = giassorb / idr
	idel = idl / gispeed
	ider = idr / gispeed
	print iampl, iampr, idel, ider
  ;
  ; calcolo delle riflessioni
  ; ;
  ; geometria della stanza
	;
  iwcornerlx  table 4, 1   ; angolo superiore sinistro (x)
  iwcornerly  table 5, 1   ; angolo superiore sinistro (y)
  iwcornerrx  table 6, 1   ; angolo inferiore destro (x)
  iwcornerry  table 7, 1   ; angolo inferiore destro (y)
  ;
  ; posizione delle riflessioni
  ;
  ilrefposxls = ((iwcornerlx-ilsx)*2)+ix ; muro sinistro -> lsp sinistro
  ilrefposxlr = ((iwcornerlx-ilrx)*2)+ix ; muro sinistro -> lsp destro
  ilrefposy   = iy
  irrefposxrs = ((iwcornerrx-ilsx)*2)+ix ; muro destro -> lsp sinistro
  irrefposxrr = ((iwcornerrx-ilrx)*2)+ix ; muro destro -> lsp destro
  irrefposy = iy
  ifrefposx = ix                ; muro frontale
  ifrefposyfs = ((iwcornerly-ilsy)*2)+iy ; muro frontale -> lsp sinistro
  ifrefposyfr = ((iwcornerly-ilry)*2)+iy ; muro frontale -> lsp destro
  ibrefposx   = ix                ; muro posteriore
  ibrefposybs = ((iwcornerry-ilsy)*2)+iy ; muro posteriore -> lsp sinistro
  ibrefposybr = ((iwcornerry-ilry)*2)+iy ; muro posteriore -> lsp destro
  print ilrefposxls, ilrefposxlr, ilrefposy, irrefposxrs, irrefposxrr, ifrefposx, ifrefposyfs, ifrefposyfr, ibrefposybs, ibrefposybr
  ;
  ; TODO: 1) calcolare tempi di ritardo e abbattimento di ampiezza
  ;          per tutte le riflessioni
  ;
  ; distanze delle riflessioni
  ; 
  idrefll = sqrt((ilrefposxls^2) + (ilrefposy^2))
  idreflr = sqrt((ilrefposxlr^2) + (ilrefposy^2))
  idrefrl = sqrt((irrefposxrs^2) + (irrefposy^2))
  idrefrr = sqrt((irrefposxrs^2) + (irrefposy^2))
  idreffl = sqrt((ifrefposx^2)   + (ifrefposyfs^2))
  idreffr = sqrt((ifrefposx^2)   + (ifrefposyfr^2))
  idrefbl = sqrt((ibrefposx^2)   + (ibrefposybs^2))
  idrefbr = sqrt((ibrefposx^2)   + (ibrefposybr^2))
  ;
  ; ritardi delle riflessioni
  ;
  idrefdelll = idrefll / gispeed
  idrefdellr = idreflr / gispeed
  idrefdelrl = idrefrl / gispeed
  idrefdelrr = idrefrr / gispeed
  idrefdelfl = idreffl / gispeed
  idrefdelfr = idreffr / gispeed
  idrefdelbl = idrefbl / gispeed
  idrefdelbr = idrefbr / gispeed
  ;
  ; abbattimento ampiezze delle riflessioni
  ;
  iarefampll = giassorb / idrefll
  iarefamplr = giassorb / idreflr
  iarefamprl = giassorb / idrefrl
  iarefamprr = giassorb / idrefrr
  iarefampfl = giassorb / idreffl
  iarefampfr = giassorb / idreffr
  iarefampbl = giassorb / idrefbl
  iarefampbr = giassorb / idrefbr

  ; 
  ;       2) calcolare il all pass (per fare il resto)
  ;


	abufl delayr 2
  ;
  ; diretto
  ;
	adirl deltap idel
	adirr deltap ider
  ;
  ; riflessioni
  ;
  arefll deltap idrefdelll
  areflr deltap idrefdellr
  arefrl deltap idrefdelrl
  arefrr deltap idrefdelrr
  areffl deltap idrefdelfl
  areffr deltap idrefdelfr
  arefbl deltap idrefdelbl
  arefbr deltap idrefdelbr
  
	delayw aout

; 
; TODO: aggiungere allpass qui 
; 

  aleft  = adirl*iampl + arefll*iarefampll + arefrl*iarefamprl + areffl*iarefampfr + arefbl*iarefampbl
  aright = adirr*iampr + areflr*iarefamplr + arefrr*iarefamprr + areffr*iarefampfr + arefbr*iarefampbr

	outs aleft, aright
	
	zacl iindex, iindex
endin
```

* [partitura](spazializzatore_4.sco):

```csound
;
;                     position        virtual room
;                     of lousp          dimension
;                   (2 earphones)
f1 0 16    -2       -0.1 0 0.1 0      -10 10 10 -10
f2 0 16384 11 1

i1  0.01 20 0.5
i11 0    30 -2 5
```
