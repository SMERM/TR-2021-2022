sr=48000
ksmps=12
nchnls=1


  instr 1
  ;
  ; lettura di file stereo
  ;
ifile=p4

aleft, aright diskin2 ifile, 1

  out aleft+aright
  endin

  instr 2
  ;
  ; lettura di file mono
  ;
ifile=p4

amono         diskin2 ifile, 1

  out amono
  endin
