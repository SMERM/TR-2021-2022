# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 07/02/2022

### Argomenti

#### Esercizio 2: discussione finale dei singoli progetti

* Progetti consegnati: Vesprini, Timo
* Esposizione delle singole idee progettuali
* Elaborazione algoritmica delle singole idee progettuali

#### Esercizio 2: risoluzione di problematiche

##### Strumenti multipli

Orchestra:

```csound
sr=48000
ksmps=8
nchnls=1
0dbfs=1


      instr 1,2,3,4,5,6,7,8
ifreq = p4
iamp  = ampdb(p5)
idur  = p3
irise = 0.1
iindex = p1


aout  oscil iamp, ifreq, iindex
aout  linen aout, idur*irise, idur, idur*irise

      out   aout
      endin

```

Partitura

```csound
f1 0 4096 10 1
f2 0 4096 11 1
f3 0 4096 10 1 0.5 0.333 0.25 0.2
f4 0 4096 10 1
f5 0 4096 10 1
f6 0 4096 10 1
f7 0 4096 10 1
f8 0 4096 10 1 0 0.33 0 0.125

i1 0 0.5 300 -12
i1 1 0.5 350 -14
;
;
i3 0.2 0.23 100 -8
i3 0.6 2.3  121 -8
```

#### Introduzione all'Esercizio 3:

* solo onde periodiche semplici con modulazioni a piacere
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 3-6 minuti
* data di consegna: entro domenica 27 febbraio 2022 ore 13:00
* brano di riferimento: [Gyorgy Ligeti, *Artikulation*](https://youtu.be/71hNl_skTZQ)

#### [Esempio di suoni periodici semplici con modulazioni a piacere](./sinusoidi_modulate.csd)

```csound
<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1 

iamp = ampdbfs(p4)
idur = p3
i_f_init = p5
i_f_fin = p6
icorner = p7
kfreq line i_f_init, idur, i_f_fin
aout oscil iamp, kfreq, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 2

iamp = ampdbfs(p4)
idur = p3
i_f_init = p5
i_f_fin = p6
icorner = p7
kfreq expon i_f_init, idur, i_f_fin
aout oscil iamp, kfreq, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 3 

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
kvib oscil 20, ivib, 1
aout oscil iamp, ifreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 4

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
ivibamp = p8
kvibamp linseg 0, idur/4, 0, idur*0.75, ivibamp 
kvib oscil kvibamp, ivib, 1
aout oscil iamp, ifreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 5

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
ivibamp = p8
if_init = p9
if_end = p10
ipan = p11
kfreq expon if_init, idur, if_end  
kvibamp linseg 0, idur/4, 0, idur*0.75, ivibamp 
kvib oscil kvibamp, ivib, 1
aout oscil iamp, kfreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout*ipan, aout*(1-ipan)

endin
</CsInstruments>
<CsScore>
f1 0 4096 10 1

i1 0 0.5 -6 261 323 0.05
i2 1 0.5 -6 261 323 0.05
i3 2 0.5 -6 261 4 0.05
i4 3 0.5 -6 261 4 0.05 50
i5 4 5 -6 261 4 0.05 50 261 323 0.75
</CsScore>
</CsoundSynthesizer>
```

### Lavagne

![sinusoidi modulate](./sinusoidi_modulate.png)
