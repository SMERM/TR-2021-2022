sr=48000
ksmps=8
nchnls=1
0dbfs=1


      instr 1,2,3,4,5,6,7,8
ifreq = p4
iamp  = ampdb(p5)
idur  = p3
irise = 0.1
iindex = p1


aout  oscil iamp, ifreq, iindex
aout  linen aout, idur*irise, idur, idur*irise

      out   aout
      endin

