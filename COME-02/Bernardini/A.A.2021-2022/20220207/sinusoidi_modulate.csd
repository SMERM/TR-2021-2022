<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 64
nchnls = 2
0dbfs = 1

instr 1 

iamp = ampdbfs(p4)
idur = p3
i_f_init = p5
i_f_fin = p6
icorner = p7
kfreq line i_f_init, idur, i_f_fin
aout oscil iamp, kfreq, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 2

iamp = ampdbfs(p4)
idur = p3
i_f_init = p5
i_f_fin = p6
icorner = p7
kfreq expon i_f_init, idur, i_f_fin
aout oscil iamp, kfreq, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 3 

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
kvib oscil 20, ivib, 1
aout oscil iamp, ifreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 4

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
ivibamp = p8
kvibamp linseg 0, idur/4, 0, idur*0.75, ivibamp 
kvib oscil kvibamp, ivib, 1
aout oscil iamp, ifreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout, aout

endin

instr 5

iamp = ampdbfs(p4)
idur = p3
ifreq = p5
ivib = p6
icorner = p7
ivibamp = p8
if_init = p9
if_end = p10
ipan = p11
kfreq expon if_init, idur, if_end  
kvibamp linseg 0, idur/4, 0, idur*0.75, ivibamp 
kvib oscil kvibamp, ivib, 1
aout oscil iamp, kfreq + kvib, 1
aout linen aout, icorner, idur, icorner
outs aout*ipan, aout*(1-ipan)

endin
</CsInstruments>
<CsScore>
f1 0 4096 10 1

i1 0 0.5 -6 261 323 0.05
i2 1 0.5 -6 261 323 0.05
i3 2 0.5 -6 261 4 0.05
i4 3 0.5 -6 261 4 0.05 50
i5 4 5 -6 261 4 0.05 50 261 323 0.75
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
