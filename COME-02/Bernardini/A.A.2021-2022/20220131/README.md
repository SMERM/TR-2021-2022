# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 31/01/2022

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali
* Elaborazione algoritmica delle singole idee progettuali

#### Introduzione a sistemi composti di equazioni

* equazioni a più di due incognite
* equazioni lineari con condizioni d'integrazione

### Lavagne

![sistemi di equazioni](./whiteboard_vesprini_20220131.png)
