class Voce:
    def __init__(self,totdurata,durinterna,pausa,suono,start=0):
        self.totdurata=totdurata
        self.durinterna=durinterna
        self.pausa=pausa
        self.suono=suono
        self.start=start

    def fai_qualcosa(self):
        end=self.start+self.totdurata
        now=self.start
        while(now<end):
            print("i1 %8.4f %8.4f %d" % (now,self.durinterna,self.suono))
            now=now+self.durinterna+self.pausa

v1= Voce(10,2,1,33)
v1.fai_qualcosa ()

v2= Voce(10,1,2,34,1)
v2.fai_qualcosa ()
