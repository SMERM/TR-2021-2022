# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 24/01/2022

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali
* Elaborazione algoritmica delle singole idee progettuali

#### Introduzione alle maschere di tendenza

* definizione
* tipi e parametriche associate

### Lavagne

![maschere](./maschere.png)
