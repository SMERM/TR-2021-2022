# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 14/02/2022

### Argomenti

#### Esercizio 3: discussione finale dei singoli progetti

* Progresso: Vesprini, Gunn, Davami

##### `csound`: strumenti come processi - il sistema di patching `zak`

![csound strumenti processo](./csound_strumenti_processo.png)

[Orchestra](./zak_test.orc):

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

zakinit 1, 4


      instr 1,2,3,4   ; processo "rosso"
idx     = p1
idur    = p3
ifstart = p4
ifend   = p5
ivibstart = p6
ivibend   = p7
ivibamp   = 80


kgliss  expon ifstart, idur, ifend
kvibf   line  ivibstart, idur, ivibend
kviba   linseg 0, idur*0.1, 0, idur*0.9, ivibamp

kvib    oscil kviba, kvibf, 1

kout    = kgliss + kvib

        zkw kout, idx

     endin

     instr 11,12,13,14   ; processo "note in nero"
idx  = p1-10
idur = p3
iamp = ampdbfs(p4)

kfreq zkr idx

aout oscil iamp, kfreq, 1
aout linen aout, 0.05*idur, idur, 0.05*idur

    out aout
    endin
```

[Generatore di partitura](./zak_test.py)

```python
totdur = 30

print("f1 0 4096 10 1")
print("\ni1 0 %d 440 123 0.5 5" % (totdur))

metro = 138
step = 60.0/float(metro)

now=0
while(now < totdur):
    print("i11 %8.4f %8.4f -3" % (now, step))
    now += (2*step)
```

![risultato finale](./zak_test.png)

### Discussione collaterale: *aliasing* e *foldover*

![aliasing e foldover](./aliasing_foldover.png)
