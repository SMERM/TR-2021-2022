totdur = 30

print("f1 0 4096 10 1")
print("\ni1 0 %d 440 123 0.5 5" % (totdur))

metro = 138
step = 60.0/float(metro)

now=0
while(now < totdur):
    print("i11 %8.4f %8.4f -3" % (now, step))
    now += (2*step)
