sr=48000
ksmps=5
nchnls=1
0dbfs = 1

zakinit 1, 4


      instr 1,2,3,4   ; processo "rosso"
idx     = p1
idur    = p3
ifstart = p4
ifend   = p5
ivibstart = p6
ivibend   = p7
ivibamp   = 80


kgliss  expon ifstart, idur, ifend
kvibf   line  ivibstart, idur, ivibend
kviba   linseg 0, idur*0.1, 0, idur*0.9, ivibamp

kvib    oscil kviba, kvibf, 1

kout    = kgliss + kvib

        zkw kout, idx

     endin

     instr 11,12,13,14   ; processo "note in nero"
idx  = p1-10
idur = p3
iamp = ampdbfs(p4)

kfreq zkr idx

aout oscil iamp, kfreq, 1
aout linen aout, 0.05*idur, idur, 0.05*idur

    out aout
    endin
