import sys
sys.path.append('../../code')

from random import random, randint

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV7_1(Processo):

    def __init__(self, file, totdur = 60, startdur = 0.035, moduledur = 0.65, leeway = 0.005, complete = 0.8):
        super().__init__(now = leeway, totdur = totdur)
        self.suono = file
        self.startdur = startdur    # valore assoluto (sec)
        self.moduledur = moduledur  # valore assoluto (sec)
        self.leeway = leeway
        self.complete = complete

    def __run__(self):
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = self.moduledur - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            skip = self.suono.location(self.now)
            dur = self.moduledur - pause
            self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)
        dur = self.totdur - self.now
        skip = self.suono.location(self.now)
        self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
