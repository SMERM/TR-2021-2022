#
# versione 9: segmenti crescenti (1 suono) - fade in discretizzato a tempo variabile
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import sys
sys.path.append('../../code')

from aa2122lib.suono import SuonoEsterno
from v9processi import ProcessoV9_1


#
# processo 1
#
p1 = ProcessoV9_1('v9.meta')
p1.run()
