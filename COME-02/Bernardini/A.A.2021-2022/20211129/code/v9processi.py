import sys
sys.path.append('../../code')

from random import random, randint
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV9_1(Processo):

    def __init__(self, metadata):
        self.metadata = metadata
        self.load_metadata()
        super().__init__(now = self.leeway, totdur = self.totdur)
        self.moduledur = Linear(0, self.moduledur_start, self.totdur, self.moduledur_end)

    def __run__(self):
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = self.moduledur.y(self.now) - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            skip = self.suono.location(self.now)
            dur = self.moduledur.y(self.now) - pause
            self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)
        dur = self.totdur - self.now
        skip = self.suono.location(self.now)
        self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))

    def load_metadata(self):
        fh = open(self.metadata, "r")
        md = load(fh, Loader=Loader)['v9']
        self.totdur = md['totdur']
        self.startdur = md['startdur']
        self.moduledur_start = md['moduledur_start']
        self.moduledur_end = md['moduledur_end']
        self.leeway = md['leeway']
        self.complete = md['complete']
        s = md['suono']
        self.suono = SuonoEsterno(num = s['num'], dur = s['dur'], instrno = s['instrno'])
