#
# versione 7: segmenti crescenti (1 suono) - fade in discretizzato
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import sys
sys.path.append('../../code')

from aa2122lib.suono import SuonoEsterno
from v7processi import ProcessoV7_1


#
# processo 1
#
file  = SuonoEsterno(num = 2, dur = 43.374, instrno = 3)
p1 = ProcessoV7_1(file, totdur = 60.0)
p1.run()
