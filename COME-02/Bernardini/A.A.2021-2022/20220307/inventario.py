bpm = 120
u = 60 / bpm

score = [
    open("voce1.sco", "w"),
    open("voce2.sco", "w"),
    open("voce3.sco", "w"),
    open("voce4.sco", "w"),
    open("voce5.sco", "w")
]

n_score = 0

now = 0.1


class Lin:
    def __init__(self, start, end):
        self.x0 = start[0]
        self.x1 = end[0]
        self.y0 = start[1]
        self.y1 = end[1]
        self.a = self.y1 - self.y0
        self.b = self.x1 - self.x0

    def y(self, t):
        if self.a == 0 or self.b == 0:
            return self.y1
        else:
            t -= self.x0
            m = self.a / self.b
            return m * t + self.y0

    def x(self, y):
        m = self.a / self.b
        return (y - self.y0) / m


class Coord:  # crea coordinate per la classe "Maschera"; ritorna un array di punti
    def __init__(self, freq, intrv, start, dur):
        self.start = start
        self.end = start + dur
        self.freq = freq
        self.intrv = intrv
        self.ymin = self.freq * self.intrv
        self.ymax = self.freq / self.intrv
        self.a = (self.start, self.ymin)
        self.b = (self.start, self.ymax)
        self.c = (self.end, self.ymax)
        self.d = (self.end, self.ymin)

    def tri(self):
        return [self.a, self.b, self.c], (self.start, self.end)

    def rect(self):
        return [self.a, self.b, self.c, self.d], (self.start, self.end)

    def parall(self, alpha):
        if alpha > 0:
            b = (self.start + alpha, self.ymax)
            d = (self.end - alpha, self.ymin)
            return [self.a, b, self.c, d], (self.start, self.end)
        elif alpha < 0:
            a = (self.start - alpha, self.ymin)
            c = (self.end + alpha, self.ymax)
            return [a, self.b, c, self.d], (self.start, self.end)
        else:
            print("invalid steepness input \"alpha\"")
            return self.rect(), (self.start, self.end)

    def trapez(self, alpha):
        if alpha > 0:
            b = (self.start + alpha, self.ymax)
            c = (self.end - alpha, self.ymax)
            return [self.a, b, c, self.d], (self.start, self.end)
        elif alpha < 0:
            a = (self.start - alpha, self.ymin)
            d = (self.end + alpha, self.ymin)
            return [a, self.b, self.c, d], (self.start, self.end)
        else:
            print("invalid steepness input \"alpha\"")
            return self.rect(), (self.start, self.end)


class Maschera:  # solo per quadrilateri e triangoli (per ora)
    def __init__(self, coord):
        self.coordinate = coord[0]
        self.start, self.end = coord[1][0], coord[1][1]
        self.lati = self.calcola_lati()
        self.bottom = self.lati[len(self.lati) - 1]

    def calcola_lati(self):
        result = []
        for idx in range(len(self.coordinate)):
            nxt = (idx + 1) % len(self.coordinate)
            result.append(Lin(self.coordinate[idx], self.coordinate[nxt]))
        return result

    def yield_lati(self, t):
        for lato in self.lati:
            if lato.x0 <= t <= lato.x1:
                yield lato.y(t)
            else:
                yield 1

    def find_top(self, t):
        return max(self.yield_lati(t))

    def fasce(self, t, steps=2):
        top = self.find_top(t)
        q = (top/self.bottom.y(t)) ** (1/2)
        half = self.bottom.y(t) * q
        freqs_up = [half * q ** (x/steps) for x in range(1, steps+1)]
        freqs_down = [self.bottom.y(t) * q ** (x / steps) for x in range(1, steps + 1)]
        freqs_down.insert(0, self.bottom.y(t))
        return freqs_down + freqs_up


class Cnt:
    n = 0

    def __init__(self):
        self.__class__.n += 1

    def r(self):
        return self.__class__.n


class Env:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.c = Cnt().r()
        self.p = 0

    def string(self, dur):
        global now
        s = "i%d %f %f %f %f %f \n" % (self.p, now, dur, self.a, self.b, self.c)
        score[n_score].write(s)

    def line(self, dur):
        self.p = 2
        self.string(dur)
        return self.c

    def exp(self, dur):
        self.p = 3
        if self.a == 0:
            self.a += 0.0001
        else:
            pass
        self.string(dur)
        return self.c

    def log(self, dur):
        self.p = 4
        self.string(dur)
        return self.c


class Lfo:
    def __init__(self, mask, shape):
        self.mask = mask
        self.shape = shape
        self.c = Cnt().r()
        
    def string(self, dur, amp, freq, p7=0, p8=0):
        global now
        fasce = self.mask.fasce(now)
        s = "i1 %f %f %f %f %d %d %d %d \n" % (now, dur, amp, fasce[freq], self.shape, p7, p8, self.c)
        score[n_score].write(s)
# itype = 0 - sine
#
# itype = 1 - triangles
#
# itype = 2 - square (bipolar)
#
# itype = 3 - square (unipolar)
#
# itype = 4 - saw-tooth
#
# itype = 5 - saw-tooth(down)


class Oscil:
    def __init__(self, mask):
        self.mask = mask

    def string(self, dur, amp, freq, pan, p7=0, p8=0, p9=0):
        global now
        fasce = self.mask.fasce(now)
        s = "i5 %f %f %f %f %f %d %d %d \n" % (now, dur, amp, fasce[freq], pan, p7, p8, p9)
        now += dur
        score[n_score].write(s)


########################################################################################################################
m0 = Maschera(Coord(2.5, 1/2, 0, 4).rect())
m1 = Maschera(Coord(220, 3/2, 0, 4).rect())
env0 = Env(0, 0.5).line(4)
lfo0 = Lfo(m0, 0)
lfo0.string(4, 20, 4)
osc0 = Oscil(m1)
osc0.string(4, 0.5, 3, 0.5, 0, lfo0.c)

score[n_score].close()
