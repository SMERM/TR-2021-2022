sr = 48000
ksmps = 64
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

instr 1, 2, 3, 4
iindex = p1
aout oscil 1, 0.1, 2

zawm aout, iindex
endin

instr 11, 12, 13, 14
iindex = p1 -10
ix = p4
iy = p5
aout zar iindex
ilsx table 1, 1
ilsy table 2, 1
ilrx table 3, 1
ilry table 4, 1
idl = sqrt(((ix - ilsx)^2) + ((iy - ilsy)^2))
idr = sqrt(((ix - ilrx)^2) + ((iy - ilry)^2))
iampl = giassorb / idl
iampr = giassorb / idr
idel = idl / gispeed
ider = idr / gispeed
abufl delayr 2
adirl deltap idel
adirr deltap ider
delayw aout
outs adirl*iampl, adirr*iampr

zacl iindex, iindex
endin
