<CsoundSynthesizer>
<CsOptions>
-Wo ./progettoIII.wav
</CsOptions>
<CsInstruments>

sr = 48000
kr = 2
nchnls = 2
0dbfs = 1

zakinit 100, 100

instr 1
if (p7 == 0) then
	kampm = 0
elseif (p7 != 0) then
	kampm zkr p4
endif
kamp = p4+kampm

if (p8 != 0) then           ; <==== questa
	kfreqm = 0
elseif (p8 != 0) then       ; <==== e questa sono la stessa condizione!
	kfreqm zkr p8
endif
kfreq = 2.5 ;p5+kfreqm

ishape = p6
ichan = p9 
alfo lfo kamp, kfreq, ishape
zawm alfo, ichan
endin

instr 2 
istart = p4
iend = p5
idur = p3
ichan = p6
kline line istart, idur, iend
zkwm kline, ichan
endin

instr 3
istart = p4
iend = p5
idur = p3
ichan = p6
kexp expon istart, idur, iend
zkwm kexp, ichan
endin

instr 4
istart = p4
iend = p5
idur = p3
ichan = p6
klog cosseg istart, idur, iend
zkwm klog, ichan
endin

instr 5
if (p7 == 0) then
	kampm = 0
elseif (p7 != 0) then
	kampm zkr p7
endif 
kamp = p4+kampm

if (p8 == 0) then
	afreqm = 0
elseif (p8 != 0) then
	afreqm zar p8
endif
afreq = p5+afreqm

if (p9 == 0) then
	kpanm = 0
elseif (p9 != 0) then
	kpanm zkr p6
endif
kpan = p6+kpanm

aout poscil kamp, afreq
ap1, ap2 pan2 aout, kpan
outs ap1, ap2
endin
</CsInstruments>
<CsScore>
#include "voce1.sco"
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
