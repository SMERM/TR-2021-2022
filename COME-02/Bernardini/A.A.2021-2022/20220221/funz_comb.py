import numpy as np
import matplotlib.pyplot as plt


class Linear:
    def __init__(self, x0, y0, x1, y1):
        self.a = (y1-y0)/(x1-x0)
        self.b = y1 - (self.a * x1)

    def __call__(self, x):
        return self.a * x + self.b

class Expon:
    def __init__(self, x0, y0, x1, y1):
        self.a = (np.log(y1) - np.log(y0))/(x1-x0)
        self.b = np.log(y0) - (self.a*x0)

    def __call__(self, x):
        return np.exp(self.a * x + self.b)

class ModSin:
    def __init__(self, freq, amp):
        self.freq = freq
        self.amp = amp

    def __call__(self, x):
        y = np.sin(2*np.pi*self.freq(x)*x)*self.amp(x)
        return y

def to_csound_gen02(fun, size, fun_number):
    x = np.arange(0, 1, 1/size)
    y = fun
    p_fields = ' '.join([("%7.4f" % (my_y)) for my_y in y])
    result = ("f%d 0 %d 2 " % (fun_number, size)) + p_fields
    return result

amp = Expon(0, 0.5, 1, 0.01)
freq = Expon(0, 3, 1, 7)
sin = ModSin(freq, amp)
off = Linear(0, 0.8, 1, 0.2)

x = np.arange(0, 1, 0.001)

y = sin(x)+off(x)
y_off = off(x)
y_amp = amp(x) + off(x)

y_diff = y - y_off

print(to_csound_gen02(y, 1024, 1))

plt.plot(x, y, label='sin(x)+off(x)')
plt.plot(x, y_off, label='off(x)')
plt.plot(x, y_amp, label='amp(x) + off(x)')
plt.legend(fontsize=14)
plt.savefig('./funz_comb.png')
