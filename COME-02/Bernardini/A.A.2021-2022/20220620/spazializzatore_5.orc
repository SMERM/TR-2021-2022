;
; spazializzatore 5: diretto e prime riflessioni e riflessioni di ordine superiore
;
sr = 48000
ksmps = 2
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

;
; generatori
;
instr 1, 2, 3, 4
  iindex = p1
  ifreq  = p4
	aout mpulse 1, 1
	
	zawm aout, iindex
endin

;
; strumento che genera la stanza
;
instr 11, 12, 13, 14
	iindex = p1 -10
	ix = p4
	iy = p5
	aout zar iindex
  ;
  ; calcolo del suono diretto
  ;
	;
	; posizione degli altoparlanti
	;
	ilsx table 0, 1
	ilsy table 1, 1
	ilrx table 2, 1
	ilry table 3, 1
	print ilsx, ilsy, ilrx, ilry
  ;
	; calcolare la distanza del suono dagli altoparlanti
	;
	idl = sqrt(((ix - ilsx)^2) + ((iy - ilsy)^2))
	idr = sqrt(((ix - ilrx)^2) + ((iy - ilry)^2))
	print idl, idr
	iampl = giassorb / idl
	iampr = giassorb / idr
	idel = idl / gispeed
	ider = idr / gispeed
	printf_i "instr 11: iampl = %10.8f iampr = %10.8f idel = %10.8f ider = %10.8f\n", 1, iampl, iampr, idel, ider
  ;
  ; calcolo delle riflessioni
  ; ;
  ; geometria della stanza
	;
  iwcornerlx  table 4, 1   ; angolo superiore sinistro (x)
  iwcornerly  table 5, 1   ; angolo superiore sinistro (y)
  iwcornerrx  table 6, 1   ; angolo inferiore destro (x)
  iwcornerry  table 7, 1   ; angolo inferiore destro (y)
  ;
  ; posizione delle riflessioni
  ;
  ilrefposx   = (2*iwcornerlx)-ix        ; rif. muro sinistro (x)
  irrefposx   = (2*iwcornerrx)-ix        ; rif. muro destro   (x)
  ilrrefposy  = iy                       ; rif. laterali      (y)
  ifrefposy   = (2*iwcornerly)-iy        ; rif. muro frontale (y)
  ibrefposy   = (2*iwcornerry)-iy        ; rif. muro posterioree (y)
  ifbrefposx  = ix                       ; rif. fronte/retro  (x)
  printf_i "instr 11: ilrefposx = %10.8f irrefposx = %10.8f ifrefposy = %10.8f ibrefposy = %10.8f\n", 1, ilrefposx, irrefposx, ifrefposy, ibrefposy
  ;
  ; distanze delle riflessioni
  ; 
  idrefll = sqrt(((ilrefposx-ilsx)^2) + ((ilrrefposy-ilsy)^2)) ; parete sinistra -> lsp sinistro
  idreflr = sqrt(((ilrefposx-ilrx)^2) + ((ilrrefposy-ilry)^2))  ; parete sinistra -> lsp destro
  idrefrl = sqrt(((irrefposx-ilsx)^2) + ((ilrrefposy-ilsy)^2))  ; parete destra   -> lsp sinistro
  idrefrr = sqrt(((irrefposx-ilrx)^2) + ((ilrrefposy-ilry)^2))  ; parete destra   -> lsp destro
  idreffl = sqrt(((ifbrefposx-ilsx)^2) + ((ifrefposy-ilsy)^2)) ; parete fronte  -> lsp sinistro
  idreffr = sqrt(((ifbrefposx-ilrx)^2) + ((ifrefposy-ilry)^2)) ; parete fronte  -> lsp destro
  idrefbl = sqrt(((ifbrefposx-ilsx)^2) + ((ibrefposy-ilsy)^2)) ; parete post    -> lsp sinistro
  idrefbr = sqrt(((ifbrefposx-ilrx)^2) + ((ibrefposy-ilry)^2)) ; parete post    -> lsp destro
  printf_i "instr 11: idrefll = %10.8f idreflr = %10.8f idrefrl = %10.8f idrefrr = %10.8f idreffl = %10.8f idreffr = %10.8f idrefbl = %10.8f idrefbr = %10.8f\n", 1, idrefll, idreflr, idrefrl, idrefrr, idreffl, idreffr, idrefbl, idrefbr
  ;
  ; ritardi delle riflessioni
  ;
  idrefdelll = idrefll / gispeed
  idrefdellr = idreflr / gispeed
  idrefdelrl = idrefrl / gispeed
  idrefdelrr = idrefrr / gispeed
  idrefdelfl = idreffl / gispeed
  idrefdelfr = idreffr / gispeed
  idrefdelbl = idrefbl / gispeed
  idrefdelbr = idrefbr / gispeed
  ;
  ; abbattimento ampiezze delle riflessioni
  ;
  iarefampll = giassorb / idrefll
  iarefamplr = giassorb / idreflr
  iarefamprl = giassorb / idrefrl
  iarefamprr = giassorb / idrefrr
  iarefampfl = giassorb / idreffl
  iarefampfr = giassorb / idreffr
  iarefampbl = giassorb / idrefbl
  iarefampbr = giassorb / idrefbr

  ; 
  ;       2) calcolare il all pass (per fare il resto)
  ;
  ; allpass pre-delay
  ;
  iallpx = (iwcornerlx-iwcornerrx)
  iallpy = (iwcornerly-iwcornerry)
  idistallp  = sqrt((iallpx^2) + (iallpy^2))
  idelallp  = idistallp / gispeed     ; pre-delay
  iampallp  = giassorb  / idistallp   ; assorbimento


	abufl delayr 2
  ;
  ; diretto
  ;
	adirl deltap idel
	adirr deltap ider
  ;
  ; prime riflessioni
  ;
  arefll deltap idrefdelll
  areflr deltap idrefdellr
  arefrl deltap idrefdelrl
  arefrr deltap idrefdelrr
  areffl deltap idrefdelfl
  areffr deltap idrefdelfr
  arefbl deltap idrefdelbl
  arefbr deltap idrefdelbr
  aallppd deltap idelallp   ; pre-delay del allpass
  
	delayw aout


; 
aallpout alpass aallppd, 1.8, idelallp/10
; 

  aleft  = adirl*iampl + arefll*iarefampll + arefrl*iarefamprl + areffl*iarefampfr + arefbl*iarefampbl + aallpout*iampallp
  aright = adirr*iampr + areflr*iarefamplr + arefrr*iarefamprr + areffr*iarefampfr + arefbr*iarefampbr + aallpout*iampallp

	outs aleft, aright
	
	zacl iindex, iindex
endin
