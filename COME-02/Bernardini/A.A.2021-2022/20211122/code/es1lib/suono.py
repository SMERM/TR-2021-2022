class Suono:
    """
        Suono: la classe che caratterizza i suoni

        Esempio d'utilizzo:

        from es1lib.suono import Suono

        s = Suono()
    """
    def __init__(self, instrno = 1):
       self.instrno = instrno

    def print(self, at, dur, parameters = ()):
        parstring = ""
        for p in parameters:
            formato = "%8.4f"
            if type(p) is tuple:
                formato = p[1]
                dato   = p[0]
            else:
                dato = p
            formato += " "
            parstring += (formato % (dato))
        print("i%d %8.4f %8.4f %s" % (self.instrno, at, dur, parstring))

class SuonoEsterno(Suono):

    def __init__(self, num = 1, dur = 0, instrno = 1):
        super().__init__(instrno = instrno)
        self.number = num
        self.dur = dur

    def location(self, value):
        return (value % self.dur)
