#
# Oggetti matematici vari ed eventual
#

class Linear:
    """
        Linear: funzione lineare di primo grado
    
        esempio di utilizzo:
    
            l = Linear(x0, y0, x1, y1)
            y = l.y(x)
    """

    def __init__(self, x0, y0, x1, y1):
        self.a = (y1-y0)/(x1-x0)
        self.b = y0


    def y(self, x):
        return self.a*x+self.b
