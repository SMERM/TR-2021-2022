class Processo:
    """
        Processo: classe base di tutti i processi.
        Utile per derivare facilmente processi.
    """

    def __init__(self, totdur = 1, now = 0):
        self.totdur = totdur
        self.now = now

    def __run__(self):
        """ questo e` il metodo da derivare """
        pass

    def run(self):
        self.header()
        self.__run__()
        self.trailer()

    def header(self):
        print(";\n; Inizio processo %s\n;" % (self.__class__.__name__))

    def trailer(self):
        print(";\n; Fine processo %s\n;" % (self.__class__.__name__))
