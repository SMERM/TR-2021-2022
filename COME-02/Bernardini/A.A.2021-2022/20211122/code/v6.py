#
# versione 6: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono per segmenti consequenziali
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria es1lib
#

from es1lib.suono import SuonoEsterno
from v6processi import ProcessoV6_1, ProcessoV6_2

#
# processo 2 (2 processi)
#

p21 = ProcessoV6_2(instrno = 2, bus = 1, funnum = 1, fun = "0 4096 8 0.1 512 0.4 512 0.3 512 0.2  512 0.6 512 0.9 512 0.4 512 0.1 512 0.05")
p22 = ProcessoV6_2(instrno = 2, bus = 2, funnum = 2, fun = "0 4096 8 0.9 512 0.5 512 0.8 512 0.95 512 0.3 512 0.1 512 0.4 512 0.7 512 0.9")

p21.run()
p22.run()

#
# processo 1
#
files  = [ SuonoEsterno(num = 1, dur = 22.831), SuonoEsterno(num = 2, dur = 43.374) ]
p1 = ProcessoV6_1(files, totdur = 60)
p1.run()
