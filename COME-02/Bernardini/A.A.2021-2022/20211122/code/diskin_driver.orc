;
; diskin driver n.2 per l'esercizio 1
;
; esempio di strumenti "strumento" e strumenti "processo"
;
; da modificare liberamente!
;
sr = 44100
ksmps=5
nchnls=1

zakinit   2,2       ; 2 bus a-rate, 2 bus k-rate

          instr 1   ; questo è uno strumento "strumento"

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
ibus      =   ifile
iskip     =   p6
icorner   =   0.015

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          zaw     aout, ibus

          endin

          instr 2    ; questo è uno strumento "processo"
idur      =    p3
ifun      =    p4
ibus      =    p5

kamp      oscil1i 0, 1, idur, ifun
araw      zar  ibus
acooked   =    araw * kamp

          out  acooked  

          endin
