from random import random, randint

from es1lib.suono import SuonoEsterno, Suono
from es1lib.processo import Processo
from es1lib.math import Linear

class ProcessoV6_1(Processo):

    def __init__(self, files, totdur = 60, startdur = 0.035, enddur = 0.6, durrng = 0.1, skiprng = 0.01, minamp=-8, maxamp=-2, leeway=0.03):
        super().__init__(now = leeway, totdur = totdur) # passiamo gli argomenti alla classe base
        self.files = files
        self.startdur = startdur
        self.enddur = enddur
        self.durrng = durrng
        self.skiprng = skiprng
        self.minamp = minamp
        self.maxamp = maxamp
        self.leeway = leeway
        self.halflw = self.leeway/2.0
        self.durfun = Linear(0, self.startdur, self.totdur, self.enddur)

    def __run__(self):
        while(self.now < self.totdur):
            fileidx = randint(0, 1)
            suono = self.files[fileidx]
            dur_nominale = self.durfun.y(self.now)
            min_dur = dur_nominale * (1-self.durrng)
            max_dur = dur_nominale * (1+self.durrng)
            rng_dur = max_dur-min_dur
            rng_amp = self.maxamp - self.minamp
            dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
            amp = random()*rng_amp+self.minamp   # ampiezza random tra minamp e maxamp
            ourskip = self.now+(random()*self.skiprng*2-self.skiprng) # collocazione corrente...
            skip = suono.location(ourskip)  # ...modulo la durata del file
            suono.print(self.now-self.halflw, dur+self.leeway, ((amp, "%+6.2f"), (suono.number, "%d"), skip))
            self.now += dur

class ProcessoV6_2(Processo):

    def __init__(self, totdur = 60, fun = "", instrno = 1, bus = 0, funnum = 1):
        super().__init__(totdur = totdur) # passiamo gli argomenti alla classe base
        self.funnum = funnum
        self.fun = ("f%d " % (self.funnum)) + fun
        self.instr = Suono(instrno = instrno)
        self.leeway = 0.5
        self.bus = bus

    def header(self):
        super().header()
        print(self.fun)
        print(";\n;")

    def __run__(self):
        self.instr.print(self.now, self.totdur+self.leeway, ((self.funnum, "%d"), (self.bus, "%d")))
