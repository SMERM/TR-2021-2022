# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 22/11/2021

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali

###### Esempi di estensioni orchestrali

* Strumenti `strumento` e strumenti `processo`

[diskin_driver.orc](./code/diskin_driver.orc)
```csound
;
; diskin driver n.2 per l'esercizio 1
;
; esempio di strumenti "strumento" e strumenti "processo"
;
; da modificare liberamente!
;
sr = 44100
ksmps=5
nchnls=1

zakinit   2,2       ; 2 bus a-rate, 2 bus k-rate

          instr 1   ; questo è uno strumento "strumento"

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
ibus      =   ifile
iskip     =   p6
icorner   =   0.015

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          zaw     aout, ibus

          endin

          instr 2    ; questo è uno strumento "processo"
idur      =    p3
ifun      =    p4
ibus      =    p5

kamp      oscil1i 0, 1, idur, ifun
araw      zar  ibus
acooked   =    araw * kamp

          out  acooked  

          endin
```

###### v6: realizzazione "pro" in `python`

* il codice diventa tutto *ad oggetti*
* si costruisce una libreria (`es1lib`) di oggetti riutilizzabili
* si crea una prima realizzazione completa

####### Libreria `es1lib`

[suono.py](./code/es1lib/suono.py)

```python
class Suono:
    """
        Suono: la classe che caratterizza i suoni

        Esempio d'utilizzo:

        from es1lib.suono import Suono

        s = Suono()
    """
    def __init__(self, instrno = 1):
       self.instrno = instrno

    def print(self, at, dur, parameters = ()):
        parstring = ""
        for p in parameters:
            formato = "%8.4f"
            if type(p) is tuple:
                formato = p[1]
                dato   = p[0]
            else:
                dato = p
            formato += " "
            parstring += (formato % (dato))
        print("i%d %8.4f %8.4f %s" % (self.instrno, at, dur, parstring))

class SuonoEsterno(Suono):

    def __init__(self, num = 1, dur = 0, instrno = 1):
        super().__init__(instrno = instrno)
        self.number = num
        self.dur = dur

    def location(self, value):
        return (value % self.dur)
```

[math.py](./code/es1lib/math.py)
```python
#
# Oggetti matematici vari ed eventual
#

class Linear:
    """
        Linear: funzione lineare di primo grado
    
        esempio di utilizzo:
    
            l = Linear(x0, y0, x1, y1)
            y = l.y(x)
    """

    def __init__(self, x0, y0, x1, y1):
        self.a = (y1-y0)/(x1-x0)
        self.b = y0


    def y(self, x):
        return self.a*x+self.b
```

[processo.py](./code/es1lib/processo.py)
```python
class Processo:
    """
        Processo: classe base di tutti i processi.
        Utile per derivare facilmente processi.
    """

    def __init__(self, totdur = 1, now = 0):
        self.totdur = totdur
        self.now = now

    def __run__(self):
        """ questo e` il metodo da derivare """
        pass

    def run(self):
        self.header()
        self.__run__()
        self.trailer()

    def header(self):
        print(";\n; Inizio processo %s\n;" % (self.__class__.__name__))

    def trailer(self):
        print(";\n; Fine processo %s\n;" % (self.__class__.__name__))
```

####### Realizzazione pratica

* un file con oggetti intermedi (la *colla* tra il software e la libreria)
[v6processi.py](./code/v6processi.py)
```python
from random import random, randint

from es1lib.suono import SuonoEsterno, Suono
from es1lib.processo import Processo
from es1lib.math import Linear

class ProcessoV6_1(Processo):

    def __init__(self, files, totdur = 60, startdur = 0.035, enddur = 0.6, durrng = 0.1, skiprng = 0.01, minamp=-8, maxamp=-2, leeway=0.03):
        super().__init__(now = leeway, totdur = totdur) # passiamo gli argomenti alla classe base
        self.files = files
        self.startdur = startdur
        self.enddur = enddur
        self.durrng = durrng
        self.skiprng = skiprng
        self.minamp = minamp
        self.maxamp = maxamp
        self.leeway = leeway
        self.halflw = self.leeway/2.0
        self.durfun = Linear(0, self.startdur, self.totdur, self.enddur)

    def __run__(self):
        while(self.now < self.totdur):
            fileidx = randint(0, 1)
            suono = self.files[fileidx]
            dur_nominale = self.durfun.y(self.now)
            min_dur = dur_nominale * (1-self.durrng)
            max_dur = dur_nominale * (1+self.durrng)
            rng_dur = max_dur-min_dur
            rng_amp = self.maxamp - self.minamp
            dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
            amp = random()*rng_amp+self.minamp   # ampiezza random tra minamp e maxamp
            ourskip = self.now+(random()*self.skiprng*2-self.skiprng) # collocazione corrente...
            skip = suono.location(ourskip)  # ...modulo la durata del file
            suono.print(self.now-self.halflw, dur+self.leeway, ((amp, "%+6.2f"), (suono.number, "%d"), skip))
            self.now += dur

class ProcessoV6_2(Processo):

    def __init__(self, totdur = 60, fun = "", instrno = 1, bus = 0, funnum = 1):
        super().__init__(totdur = totdur) # passiamo gli argomenti alla classe base
        self.funnum = funnum
        self.fun = ("f%d " % (self.funnum)) + fun
        self.instr = Suono(instrno = instrno)
        self.leeway = 0.5
        self.bus = bus

    def header(self):
        super().header()
        print(self.fun)
        print(";\n;")

    def __run__(self):
        self.instr.print(self.now, self.totdur+self.leeway, ((self.funnum, "%d"), (self.bus, "%d")))
```

* un *application driver*

[v6.py](./code/v6.py)

```python
#
# versione 6: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono per segmenti consequenziali
# due file: 316731__jcameron__uridium-loop-action-fight.wav (22.831 secondi)
#           503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria es1lib
#

from es1lib.suono import SuonoEsterno
from v6processi import ProcessoV6_1, ProcessoV6_2

#
# processo 2 (2 processi)
#

p21 = ProcessoV6_2(instrno = 2, bus = 1, funnum = 1, fun = "0 4096 8 0.1 512 0.4 512 0.3 512 0.2  512 0.6 512 0.9 512 0.4 512 0.1 512 0.05")
p22 = ProcessoV6_2(instrno = 2, bus = 2, funnum = 2, fun = "0 4096 8 0.9 512 0.5 512 0.8 512 0.95 512 0.3 512 0.1 512 0.4 512 0.7 512 0.9")

p21.run()
p22.run()

#
# processo 1
#
files  = [ SuonoEsterno(num = 1, dur = 22.831), SuonoEsterno(num = 2, dur = 43.374) ]
p1 = ProcessoV6_1(files, totdur = 60)
p1.run()
```

### Lavagne usata durante la lezione

![whiteboard 1](./Lavagne 11-23-2021 08.10_1.jpg)


![whiteboard 2](./Lavagne 11-23-2021 08.10_2.jpg)

### Studio a casa

* realizzazione pratica dei progetti riguardanti l'esercizio 1
