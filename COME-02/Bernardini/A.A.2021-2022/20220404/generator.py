
def iterate_over(a = []):
    for x in a:
        yield "this is: ", x

def plus_three(a = []):
    for s, n in iterate_over(a):
        print(s + str(n+3))

def mul_three(a = []):
    for s, n in iterate_over(a):
        print(s + str(n*3))


a = [1, 2, 3, 4]

plus_three(a)
mul_three(a)
