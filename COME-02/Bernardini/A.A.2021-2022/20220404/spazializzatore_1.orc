sr = 48000
ksmps = 64
nchnls = 2
0dbfs = 1

zakinit 4, 8
giassorb init 1
gispeed init 340

instr 1, 2, 3, 4
iindex = p1
aout gbuzz 1, 0.1, 10000, 1, 1, 2

zawm aout, iindex
endin

instr 11, 12, 13, 14
iindex = p1 -10
ix = p4
iy = p5
aout zar iindex
;
; leggiamo la posizione degli altoparlanti
;
ilsx table 0, 1
ilsy table 1, 1
ilrx table 2, 1
ilry table 3, 1
print ilsx, ilsy, ilrx, ilry
;
; calcolare la distanza del suono dagli altoparlanti
;
idl = sqrt(((ix - ilsx)^2) + ((iy - ilsy)^2))
idr = sqrt(((ix - ilrx)^2) + ((iy - ilry)^2))
print idl, idr
iampl = giassorb / idl
iampr = giassorb / idr
idel = idl / gispeed
ider = idr / gispeed
print iampl, iampr, idel, ider
abufl delayr 2
adirl deltap idel
adirr deltap ider
delayw aout
outs adirl*iampl, adirr*iampr

zacl iindex, iindex
endin
