
from random import random
import numpy as np


class Suono:
    def __init__(self, name, dur, amp):
        self.name= name
        self.dur= dur
        self.amp= amp
    def loop(self, t):
        return t % self.dur
    def to_csound(self, instr, at, dur, skip, amp ):
        print("i%d %8.4f %8.4f \"%s\" %8.4f %8.4f" % (instr, at, dur, self.name, self.loop(skip), self.amp))

class Hunning:
    def __init__(self, at, dur):
        self.at= at
        self.dur=dur
    def y(self, x):
        return -0.5*np.cos(((2*np.pi)/self.dur)*(x-self.at)+ 0.5)

class Nuvola:
    def __init__(self, at, ndur, maxdens):
        self.at= at
        self.ndur= ndur
        self.maxdens= maxdens
        self.funzione = Hunning(self.at, self.ndur)

    def run(self):
        end=self.at+self.ndur
        now=self.at
        suono=Suono("c1m.wav", 2.628, 0.08)
        step= 0.05  #mini step da stabilire
        graindur= (0.04, 0.4) #range del random
        while(now<end):
            dens= self.funzione.y(now)*self.maxdens

            for e in range(int(dens)):
                eat= now+(step*random())-(step/2) #random tra le fettine di step
                dur= (graindur[1]-graindur[0])*random()+graindur[0]
                skip = random()*suono.dur
                suono.to_csound(1, eat, dur, skip,0.08)
            now+=step


totdur = 90
n=Nuvola(totdur/2, totdur/4, 30)
n.run()
