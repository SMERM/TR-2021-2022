
sr = 44100
ksmps=5
nchnls=1
0dbfs = 1

 instr 1 ; mono input

idur      =   p3
iamp      =   ampdb(p6)
ifile     =   p4
iskip     =   p5
icorner   =   0.01

a1        diskin  ifile, 1, iskip
aout      linen   a1*iamp, icorner, idur, icorner

          out    aout

          endin


