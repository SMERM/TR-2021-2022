# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 06/12/2021

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali
* Elaborazione algoritmica delle singole idee progettuali

#### Metafiles

#### Orchestra driver

[diskin_driver.orc](./code/diskin_driver.orc)
```csound
;
; diskin driver n.3 per l'esercizio 1
;
; esempio di strumenti "strumento" e strumenti "processo"
;
; da modificare liberamente!
;
sr = 44100
ksmps=5
nchnls=1

zakinit   2,2       ; 2 bus a-rate, 2 bus k-rate

          instr 1   ; questo è uno strumento "strumento"

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
ibus      =   ifile
iskip     =   p6
icorner   =   0.015

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          zaw     aout, ibus

          endin

          instr 2    ; questo è uno strumento "processo"
idur      =    p3
ifun      =    p4
ibus      =    p5

kamp      oscil1i 0, 1, idur, ifun
araw      zar  ibus
acooked   =    araw * kamp

          out  acooked  

          endin

          instr 3   ; questo è uno strumento stand-alone

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
ibus      =   ifile
iskip     =   p6
icorner   =   0.015

al, ar    diskin  ifile, 1, iskip
aout      linen   (al+ar)*iamp, icorner, idur, icorner

          out    aout

          endin
```

#### Libreria come asset stabile riutilizzabile

* la libreria diventa un asset stabile ([`aa2122lib`](../code/aa2122lib/README.md))

#### Realizzazioni pratiche

##### `v7`: Fade-in "discretizzato"

[v7processi](./code/v7processi.py)

```python
import sys
sys.path.append('../../code')

from random import random, randint

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV7_1(Processo):

    def __init__(self, file, totdur = 60, startdur = 0.035, moduledur = 0.65, leeway = 0.005, complete = 0.8):
        super().__init__(now = leeway, totdur = totdur)
        self.suono = file
        self.startdur = startdur    # valore assoluto (sec)
        self.moduledur = moduledur  # valore assoluto (sec)
        self.leeway = leeway
        self.complete = complete

    def __run__(self):
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = self.moduledur - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            skip = self.suono.location(self.now)
            dur = self.moduledur - pause
            self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)
        dur = self.totdur - self.now
        skip = self.suono.location(self.now)
        self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
```

[v7](./code/v7.py)
```python
#
# versione 7: segmenti crescenti (1 suono) - fade in discretizzato
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import sys
sys.path.append('../../code')

from aa2122lib.suono import SuonoEsterno
from v7processi import ProcessoV7_1

##### `v8`: Fade-in "discretizzato" con accelerazioni invertite

[v8processi](./code/v8processi.py)

```python
import sys
sys.path.append('../../code')

from random import random, randint

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV8_1(Processo):

    def __init__(self, file, totdur = 60, startdur = 0.015, moduledur_start = 0.85, moduledur_end = 0.15, leeway = 0.005, complete = 0.8):
        super().__init__(now = leeway, totdur = totdur)
        self.suono = file
        self.startdur = startdur
        self.moduledur = Linear(0, moduledur_start, self.totdur, moduledur_end)
        self.leeway = leeway
        self.complete = complete

    def __run__(self):
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = self.moduledur.y(self.now) - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            skip = self.suono.location(self.now)
            dur = self.moduledur.y(self.now) - pause
            self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)
        dur = self.totdur - self.now
        skip = self.suono.location(self.now)
        self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
```

[v8](./code/v8.py)
```python
#
# versione 8: segmenti crescenti (1 suono) - fade in discretizzato a tempo variabile
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import sys
sys.path.append('../../code')

from aa2122lib.suono import SuonoEsterno
from v8processi import ProcessoV8_1


#
# processo 1
#
file  = SuonoEsterno(num = 2, dur = 43.374, instrno = 3)
p1 = ProcessoV8_1(file, totdur = 60)
p1.run()
```

##### `v9`: metadati

[v9 metadata](./code/v9.meta)
```yaml
v9:
  suono:
    num: 2
    dur: 43.374
    instrno: 3
  totdur: 60.0
  startdur: 0.015
  moduledur_start: 0.85
  moduledur_end: 0.15
  leeway: 0.005
  complete: 0.8
```

[v9 processi](./code/v9processi.py)
```python
import sys
sys.path.append('../../code')

from random import random, randint
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV9_1(Processo):

    def __init__(self, metadata):
        self.metadata = metadata
        self.load_metadata()
        super().__init__(now = self.leeway, totdur = self.totdur)
        self.moduledur = Linear(0, self.moduledur_start, self.totdur, self.moduledur_end)

    def __run__(self):
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = self.moduledur.y(self.now) - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            skip = self.suono.location(self.now)
            dur = self.moduledur.y(self.now) - pause
            self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)
        dur = self.totdur - self.now
        skip = self.suono.location(self.now)
        self.suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (self.suono.number, "%d"), skip))

    def load_metadata(self):
        fh = open(self.metadata, "r")
        md = load(fh, Loader=Loader)['v9']
        self.totdur = md['totdur']
        self.startdur = md['startdur']
        self.moduledur_start = md['moduledur_start']
        self.moduledur_end = md['moduledur_end']
        self.leeway = md['leeway']
        self.complete = md['complete']
        s = md['suono']
        self.suono = SuonoEsterno(num = s['num'], dur = s['dur'], instrno = s['instrno'])
```

[v9 driver](./code/v9.py)
```python
#
# versione 9: segmenti crescenti (1 suono) - fade in discretizzato a tempo variabile
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import sys
sys.path.append('../../code')

from aa2122lib.suono import SuonoEsterno
from v9processi import ProcessoV9_1


#
# processo 1
#
p1 = ProcessoV9_1('v9.meta')
p1.run()
```

<!--
### Lavagna usata durante la lezione

![whiteboard](./whiteboard_20211122.png)
-->

### Studio a casa

* realizzazione pratica dei progetti riguardanti l'esercizio 1
