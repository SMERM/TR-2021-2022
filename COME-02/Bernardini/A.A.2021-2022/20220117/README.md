# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 17/01/2022

### Argomenti

#### Esercizio 1: valutazione dei progetti

| Studente           | Mus. | RT   | Q.S. | Punt. | Punteggio |
|--------------------|:----:|:----:|:----:|:-----:|:---------:|
| Babak Davami       |      |      |      |       |           |
| Ilaria Gunn        |      |      |      |       |           |
| Francesco Vesprini |      |      |      |       |           |
| Valerio Timo       |      |      |      |       |           |

#### Esercizio 2: introduzione

* solo onde periodiche semplici (non modulate - solo inviluppi trapezoidali)
* vietato utilizzare audio editors
* utilizzare `csound` o `supercollider` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 2-5 minuti
* data di consegna: entro domenica 7 febbraio 2022 ore 13:00
* brano di riferimento: [Fausto Razzi, *Progetto II*](https://open.spotify.com/track/61LcHYVkacI16YhUjXAivk)

#### Esercizio 2: discussione dei dettagli

* singole voci
* distribuzione delle frequenze?
* distribuzione delle durate?
* distribuzione degli attacchi?
