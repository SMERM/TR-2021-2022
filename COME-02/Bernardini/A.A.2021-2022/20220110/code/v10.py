#
# versione 9: segmenti crescenti (1 suono) - fade in discretizzato a tempo variabile
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import pdb
import sys
sys.path.append('../../code')

from aa2122lib.meta import load_metadata

#
# processo 1
#
ps = load_metadata('v10.meta')
p1 = ps[0]
p1.run()
