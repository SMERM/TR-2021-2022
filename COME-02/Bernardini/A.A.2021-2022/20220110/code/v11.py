#
# versione 11: meta partitura
# un file:    503504__kirmm__church-choir.wav                 (43.374 secondi)
#
# Realizzazione con la libreria aa2122lib
#
import pdb
import sys
sys.path.append('../../code')

from aa2122lib.meta import load_metadata

#
# processo 1
#
ps = load_metadata('v11.meta')
p1 = ps[0]
p1.print()
