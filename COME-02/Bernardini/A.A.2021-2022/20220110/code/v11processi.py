import sys
sys.path.append('../../code')

from random import random, randint
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from aa2122lib.suono import SuonoEsterno, Suono
from aa2122lib.processo import Processo
from aa2122lib.math import Linear

class ProcessoV11(Processo):

    def __run__(self):
        result = ''
        moduledur = Linear(0, self.moduledur_start, self.totdur, self.moduledur_end)
        halflw = self.leeway/2.0
        pdur = self.totdur * self.complete
        pause = moduledur.y(self.now) - self.startdur
        tfun = Linear(0, pause, pdur, 0)
        amp = -8
        while(pause > 0):
            sidx = randint(0,1)
            suono = self.suoni[sidx]
            skip = suono.location(self.now)
            dur = moduledur.y(self.now) - pause
            result += suono.print(self.now-halflw, dur+self.leeway, ((amp, "%+6.2f"), (suono.soundin, "%d"), skip))
            self.now += (dur + pause)
            pause = tfun.y(self.now)

        return result
