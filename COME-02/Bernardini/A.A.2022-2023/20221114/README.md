# Composizione Musicale Elettroacustica II (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 14/11/2022

### Argomenti

* Discussione dell'esercizio 1:
  * scelta dello strumento
  * approccio alla composizione
  * discussione sulla notazione
    * partitura d'ascolto
    * partitura di esecuzione
    * strumenti semiografici:
      * `lilypond`
      * `pic`
