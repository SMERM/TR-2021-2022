# Composizione Musicale Elettroacustica II (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 12/12/2022

### Argomenti

#### Discussione dell'esercizio 1:
  * discussione delle scelte compositive:
    * analisi vocale (Timo)
    * deploy seriale (Davami)
    * mapping astratto/concreto (Vesprini)
  * discussione sulla notazione
    * partitura d'ascolto
    * partitura di esecuzione
    * strumenti semiografici:
      * `lilypond`
      * `pic`

![whiteboard 1 - principi seriali](./TR_II_12-12-2022_13.04_1.jpg)

#### Piccola deviazione tematica sulla scomposizione in serie di Fourier (continua):

![whiteboard 2 - DFT parameters](./TR_II_12-12-2022_13.04_2.jpg)

[Codice python `dft 2.py`](./dft 2.py)
```python
import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5
t = np.linspace(0, 1, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)


F = np.linspace(-(sr / 2) + 1, sr / 2, sr)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * y
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft2_output.png")
plt.show()
```

Questo codice produce il grafico seguente:

![dft 2 output](./dft2_output.png)

[Codice python `dft 3.py`](./dft 3.py)
```python
import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5.45
t = np.linspace(0, 1, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)


F = np.linspace(-(sr / 2) + 1, sr / 2, sr)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * y
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft3_output.png")
plt.show()
```

Questo codice produce il grafico seguente:

![dft 3 output](./dft3_output.png)

[Codice python `dft 4.py`](./dft 4.py)
```python
import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5.45
t = np.linspace(0, 1, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)
h = np.hanning(len(y))  # finestra Von Han
hy = y*h

F = np.linspace(-(sr / 2) + 1, sr / 2, sr)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * hy
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft4_output.png")
plt.show()
```

Questo codice produce il grafico seguente:

![dft 4 output](./dft4_output.png)

[Codice python `dft 5.py`](./dft 5.py)
```python
import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5.45
t = np.linspace(0, 2, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)
h = np.hanning(len(y))  # finestra Von Han
hy = y*h

F = np.linspace(-(sr / 2) + 0.5, sr / 2, sr*2)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * hy
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft5_output.png")
plt.show()
```

Questo codice produce il grafico seguente:

![dft 5 output](./dft5_output.png)
