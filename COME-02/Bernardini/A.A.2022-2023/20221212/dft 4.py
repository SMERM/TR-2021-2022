import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5.45
t = np.linspace(0, 1, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)
h = np.hanning(len(y))  # finestra Von Han
hy = y*h

F = np.linspace(-(sr / 2) + 1, sr / 2, sr)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * hy
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft4_output.png")
plt.show()
