import numpy as np
import matplotlib.pyplot as plt

sr = 1000  # campionamento
s_inc = 1 / sr
f = 5
t = np.linspace(0, 1, sr)
i = 1j

y = 0.6 * np.cos(2 * np.pi * f * t)


F = np.linspace(-(sr / 2) + 1, sr / 2, sr)
dft = np.zeros(F.shape, dtype=complex)

for fidx in range(len(F)):
    fa = F[fidx]
    anl = np.exp(-2 * np.pi * fa * t * i)  # funzione analitica
    temp = anl * y
    dft[fidx] = np.abs(np.sum(temp)) / len(t)

plt.cla()
plt.stem(F, dft)
plt.axis([-10, 10, -0.1, 0.7])
plt.savefig("./dft2_output.png")
plt.show()
