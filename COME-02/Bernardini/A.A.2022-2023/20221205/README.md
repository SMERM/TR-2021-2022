# Composizione Musicale Elettroacustica II (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 05/12/2022

### Argomenti

* Discussione dell'esercizio 1:
  * scelta dello strumento
  * approccio alla composizione
  * discussione sulla notazione
    * partitura d'ascolto
    * partitura di esecuzione
    * strumenti semiografici:
      * `lilypond`
      * `pic`

* Piccola deviazione tematica sulla scomposizione in serie di Fourier:

![whiteboard 1](./TR_II_12-05-2022_11.01_1.jpg)

![whiteboard 2](./TR_II_12-05-2022_11.01_2.jpg)

![whiteboard 3](./TR_II_12-05-2022_11.01_3.jpg)

[Codice python `dft.py`]
```python
import numpy as np
import matplotlib.pyplot as plt

sr= 1000 #campionamento
s_inc= 1/sr
f= 5
t= np.linspace(0,1,sr)
i= 1j

y= 0.6*np.exp((2*np.pi*f*t)*i)

plt.plot(t,np.real(y),t,np.imag(y))
#plt.show()


F= np.linspace(-(sr/2)+1,sr/2, sr)
dft= np.zeros(F.shape,dtype=complex) 

for fidx in range(len(F)):
    fa=F[fidx]
    anl=np.exp(-2*np.pi*fa*t*i) #funzione analitica
    temp=anl*y
    dft[fidx]=np.abs(np.sum(temp))/len(t)
    
plt.cla()
plt.stem(F,dft)
plt.axis([-10,10,-0.1,0.7])
plt.savefig("./dft_output.png")
plt.show()
```

Questo codice produce il grafico seguente:

![dft output](./dft_output.png)
