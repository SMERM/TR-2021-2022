# Composizione Musicale Elettroacustica II (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 16/01/2023

### Argomenti

#### Discussione dell'esercizio 1:
  * discussione delle scelte compositive:
    * analisi vocale (Timo)
    * deploy seriale (Davami)
    * mapping astratto/concreto (Vesprini)
  * discussione sulla notazione
    * partitura d'ascolto
    * partitura di esecuzione
    * strumenti semiografici:
      * `lilypond`
      * `pic`
