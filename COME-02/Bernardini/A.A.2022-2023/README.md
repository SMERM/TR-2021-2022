# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
# Seconda annualità (A.A.2022-2023)

## Macro-esercitazioni (Milestones)

### Attribuzione del punteggio degli esercizi

| Caratteristica            | Punteggio |
|---------------------------|-----------|
| Musicalità d'insieme      | ± 3 punti |
| Realizzazione tecnica     | ± 1 punto |
| Qualità sonora            | ± 1 punto |
| Puntualità della consegna | ± 1 punto |

Ogni esercizio con uno strumento diverso

### Esercizio 1: strumento e fixed media - tape music (± 6 punti)

* strumento dal vivo ed elaborazione dello stesso strumento su fixed media
* utilizzare `csound` per l'elaborazione
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori
* durata: 5-8 minuti
* data di consegna: entro domenica 22 gennaio 2023 ore 13:00
* brano di riferimento: [Bruno Maderna, *Musica su due dimensioni* (1958)](https://youtu.be/T28rO9kcNqY)

### Esercizio 2: strumento e fixed media - elettronica a contrasto (± 6 punti)

* strumento dal vivo ed elettronica "astratta" a contrasto
* utilizzare `csound` per la generazione/elaborazione 
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 5-8 minuti
* data di consegna: entro domenica 26 febbraio 2023 ore 13:00
* brano di riferimento: [Karlheinz Stockhausen *Kontakte* (1960)](https://youtu.be/l_UHaulsw3M)

### Esercizio 3: strumento ed elementi concreti su fixed media (± 6 punti)

* strumento dal vivo ed elettronica "concreta" 
* utilizzare `csound` per la generazione/elaborazione
* canali d'uscita: 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 5-8 minuti
* data di consegna: entro domenica 2 aprile 2023 ore 13:00
* brano di riferimento: [Pierre Jodlowsky *Time and Money* (2004)](https://youtu.be/xXW7oetYcvk)

### Esercizio 4: strumento solo ed elettronica implicita (± 6 punti)

* strumento solo con scrittura "elettronica"
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 5-8 minuti
* data di consegna: entro domenica 7 maggio 2023 ore 13:00
* brano di riferimento: [Gérard Grisey, *Anubis nout* (1983)](https://youtu.be/uWKDkeYNk-Y)

### Esercizio 5: brano d'esame  (± 6 punti)

* brano per strumento e fixed media
* durata: 5-10 minuti
* data di consegna: data dell'appello d'esame

# Diario di Bordo

| Studente           | Es.1 | Es.2 | Es.3 | Es.4 | Esame | Voto |
|--------------------|:----:|:----:|:----:|:----:|:-----:|:----:|
| Babak Davami       |  6   |  6   |  6   |  5   |       |      |
| Francesco Vesprini |  6   |  6   |  6   |  5   |       |      |
