# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 20/11/2023

### Argomenti

* organizzazione *musicale* di ritardi *multi-tap*
* principi di spazializzazione
* introduzione a `supercollider`
* `supercollider`: ritardi multi-tap e *pitch-shifters* (secondo micro-esercizio)

### Lavagne

![multiple_delays.png](./multiple_delays.png)

![spazializzazione](./spazializzazione.png)

![sc structure](./sc_structure.png)

### Codice 

[simple_audio_through.scd](./simple_audio_through.scd)
```sclang
//
// a simple audio through patch
//
(
    var through, gain;
    SynthDef("audiothrough", {
	     arg amp = 0;
	     var in = SoundIn.ar(bus: 0);

	     Out.ar(0, in*amp);
   }).load(s);
   through = Synth("audiothrough");
   gain = EZSlider.new(nil, 390@20, " gain ",
		               \db.asSpec.step_(0.01),
	                   { |ez| var samp = ez.value.dbamp; through.set(\amp, samp) },
	                   valueAction: 0);

)
```
