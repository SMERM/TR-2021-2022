# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 27/11/2023

### Argomenti

* `supercollider`: ritardi multi-tap e *pitch-shifters* (secondo micro-esercizio)

### Lavagne

![whiteboard 1](./whiteboard_1.png)

### Codice 

[simple_delay.scd](./simple_delay.scd)
```sclang
//
// a simple delay patch
//
(
    var simpleDelay, gain, sdel, win, close;
    var fun = {
	     arg amp = 0, delay = 0.2;
	     var in = SoundIn.ar(bus: 0);
	     var del = DelayC.ar(in: in, maxdelaytime: 10.0, delaytime: delay);

	     Out.ar(0, del*amp);
   };
   SynthDef("simpledelay", fun).play(s);
   simpleDelay = Synth("simpledelay", amp: 1, delay: 3);

   win = Window.new("delaystrafico", Rect(200,100,530,500));
   win.view.decorator = FlowLayout(win.view.bounds, 10@10, 5@5);
   gain = EZSlider.new(win, 390@20, " gain ",
		               \db.asSpec.step_(0.01),
	{ |ez| var samp = ez.value.dbamp; simpleDelay[0].set(\amp, samp) },
	                   valueAction: 0);
   sdel = EZSlider.new(win, 390@20, " delay ",
	                   ControlSpec.new(0.1, 10.0, step: 0.01),
	{ |ez| var dl = ez.value; simpleDelay[0].set(\delay, dl) },
	                   valueAction: 0.2);
   close = {win.close};
   CmdPeriod.add(close);
   win.front;
)
```
