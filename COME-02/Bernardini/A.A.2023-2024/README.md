# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
# Terza annualità (A.A.2023-2024)

## Micro-esercitazioni

### Micro-esercizio n.1 (6/11/2023):

* introduzione alle problematiche live electronics (causalità, interpretazione, scrittura, prove e simulazioni)
* introduzione agli strumenti (`pure data`, `supercollider`, `faust`)
* primo esercizio: `pure data` ritardi multi-tap e *pitch-shifters*

### Micro-esercizio n.1bis (13/11/2023):

* verifica primo micro-esercizio

### Micro-esercizio n.2 (20/11/2023):

* introduzione a `supercollider`
* secondo esercizio: `supercollider` ritardi multi-tap e *pitch-shifters*

### Micro-esercizio n.2bis (27/11/2023):

* verifica secondo micro-esercizio
* introduzione a `faust`

### Micro-esercizio n.3 (04/12/2023):

* terzo esercizio: `faust` ritardi multi-tap e *pitch-shifters*
* integrazione `faust` -> `pure data`, `supercollider`

### Micro-esercizio n.4 (11/12/2023):

* quarto esercizio: tecniche miste - distorsione e filtraggio

## Macro-esercitazioni (Milestones)

### Attribuzione del punteggio degli esercizi

| Caratteristica            | Punteggio |
|---------------------------|-----------|
| Musicalità d'insieme      | + 3 punti |
| Realizzazione tecnica     | + 1 punto |
| Qualità sonora            | + 1 punto |
| Puntualità della consegna | + 1 punto |

### Esercizio 1: strumento e live electronics semplice (+ 6 punti)

* strumento dal vivo ed elaborazione semplice (ritardi/pitch shifters)
* utilizzare `pure data` o `supercollider` + `faust` per l'elaborazione
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 18 febbraio 2024 ore 18
* brano di riferimento: [Luigi Nono, *Post-prae Ludium per Donau* (1987)](https://youtu.be/xMEh2aPncPM)

### Esercizio 2: strumento e live electronics sofisticato (+ 6 punti)

* strumento dal vivo ed elaborazione sofisticata (DNL/filtri)
* utilizzare `pure data` o `supercollider` + `faust` per l'elaborazione
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 17 marzo ore 18

### Esercizio 3: strumenti ed live-electronics incrociata (+ 6 punti)

* strumenti dal vivo e cross-control live 
* utilizzare `pure data` o `supercollider` + `faust` per l'elaborazione
* canali d'uscita: 2
* durata: 5-8 minuti
* data di consegna: entro domenica 14 aprile 2024 ore 18

### Esercizio 4: strumenti e live-electronics distorsione + filtraggio (+ 6 punti)

* strumenti e live-electronics distorsione + filtraggio
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 12 maggio 2024 ore 18

### Esercizio 5: brano d'esame  (± 6 punti)

* brano per strumento e live electronics
* durata: 5-10 minuti
* data di consegna: data dell'appello d'esame

# Diario di Bordo

| Studente           | Es.1 | Es.2 | Es.3 | Es.4 | Esame | Voto |
|--------------------|:----:|:----:|:----:|:----:|:-----:|:----:|
| Babak Davami       |      |      |      |      |       |      |
| Francesco Vesprini |      |      |      |      |       |      |
