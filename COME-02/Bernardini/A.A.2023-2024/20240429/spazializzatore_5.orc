;
; spazializzatore 5: diretto e prime riflessioni e riflessioni di ordine superiore
;
sr = 48000
ksmps = 2
nchnls = 2
0dbfs = 1

;
; 4 bus audio, 8 bus di controllo (perché per ciascun segnale
;   audio occorrono due coordinate di controllo)
;
zakinit 4, 8
giassorb init 1
gispeed init 340

;
; generatori
;
instr 1, 2, 3, 4
  iindex = p1
  ifreq  = p4
  iamp   = ampdbfs(p5)
	aout mpulse iamp, 1/ifreq
	
	zawm aout, iindex
endin

;
; controlli di spazio
;
;
; controllo lineare di spazio: questo strumento serve
; per muovere linearmente una sorgente da una parte all'altra
;
instr 11, 13, 15, 17
  iindex = p1 - 10
  idur = p3
  istartx = p4
  iendx   = p6
  istarty = p7
  iendy   = p8

  kx 	line istartx,idur,iendx
  ky    line istarty,idur,iendy

  zkw   kx, iindex
  zkw   ky, iindex+1

endin
;
; controllo circolare di spazio: questo strumento serve
; per muovere circolarmente una sorgente da una parte all'altra
;
instr 21, 23, 25, 27
  iindex = p1 - 20
  idur = p3
  ifreq = p4
  imodulo = p5 ; volendo spiralizzare il movimento si mette un line sul modulo
  iphase  = p6
  ifun    = 2

  kx	oscili  imodulo, ifreq, ifun, 0 ; fase 0 per il seno
  ky    oscili  imodulo, ifreq, ifun, 0.25 ; fase pi/2 per il coseno

  zkw   kx, iindex
  zkw   ky, iindex+1

endin
; 
;
; strumento che genera la stanza
;
instr 101, 102, 103, 104
	iindex = p1 -100
	ain zar iindex
	;
	; posizione della sorgente
	;
	kx zkr iindex
	ky zkr iindex+1
	;
	; posizione degli altoparlanti
	;
	ilsx table 0, 1
	ilsy table 1, 1
	ilrx table 2, 1
	ilry table 3, 1
	print ilsx, ilsy, ilrx, ilry
  ;
  ; calcolo del suono diretto
  ;
  ;
	; calcolare la distanza del suono dagli altoparlanti
	;
	kdl = sqrt(((kx - ilsx)^2) + ((ky - ilsy)^2))
	kdr = sqrt(((kx - ilrx)^2) + ((ky - ilry)^2))
	kampl = giassorb / kdl
	kampr = giassorb / kdr
	kdel = kdl / gispeed
	kder = kdr / gispeed
	; printf_i "instr 11: iampl = %10.8f iampr = %10.8f idel = %10.8f ider = %10.8f\n", 1, iampl, iampr, idel, ider
  ;
  ; calcolo delle riflessioni
  ; ;
  ; geometria della stanza
	;
  iwcornerlx  table 4, 1   ; angolo superiore sinistro (x)
  iwcornerly  table 5, 1   ; angolo superiore sinistro (y)
  iwcornerrx  table 6, 1   ; angolo inferiore destro (x)
  iwcornerry  table 7, 1   ; angolo inferiore destro (y)
  ;
  ; posizione delle riflessioni
  ;
  klrefposx   = (2*iwcornerlx)-kx        ; rif. muro sinistro (x)
  krrefposx   = (2*iwcornerrx)-kx        ; rif. muro destro   (x)
  klrrefposy  = ky                       ; rif. laterali      (y)
  kfrefposy   = (2*iwcornerly)-ky        ; rif. muro frontale (y)
  kbrefposy   = (2*iwcornerry)-ky        ; rif. muro posterioree (y)
  kfbrefposx  = kx                       ; rif. fronte/retro  (x)
  ; printf_i "instr 11: ilrefposx = %10.8f irrefposx = %10.8f ifrefposy = %10.8f ibrefposy = %10.8f\n", 1, ilrefposx, irrefposx, ifrefposy, ibrefposy
  ;
  ; distanze delle riflessioni
  ; 
  kdrefll = sqrt(((klrefposx-ilsx)^2) + ((klrrefposy-ilsy)^2)) ; parete sinistra -> lsp sinistro
  kdreflr = sqrt(((klrefposx-ilrx)^2) + ((klrrefposy-ilry)^2))  ; parete sinistra -> lsp destro
  kdrefrl = sqrt(((krrefposx-ilsx)^2) + ((klrrefposy-ilsy)^2))  ; parete destra   -> lsp sinistro
  kdrefrr = sqrt(((krrefposx-ilrx)^2) + ((klrrefposy-ilry)^2))  ; parete destra   -> lsp destro
  kdreffl = sqrt(((kfbrefposx-ilsx)^2) + ((kfrefposy-ilsy)^2)) ; parete fronte  -> lsp sinistro
  kdreffr = sqrt(((kfbrefposx-ilrx)^2) + ((kfrefposy-ilry)^2)) ; parete fronte  -> lsp destro
  kdrefbl = sqrt(((kfbrefposx-ilsx)^2) + ((kbrefposy-ilsy)^2)) ; parete post    -> lsp sinistro
  kdrefbr = sqrt(((kfbrefposx-ilrx)^2) + ((kbrefposy-ilry)^2)) ; parete post    -> lsp destro
  ; printf_i "instr 11: idrefll = %10.8f idreflr = %10.8f idrefrl = %10.8f idrefrr = %10.8f idreffl = %10.8f idreffr = %10.8f idrefbl = %10.8f idrefbr = %10.8f\n", 1, idrefll, idreflr, idrefrl, idrefrr, idreffl, idreffr, idrefbl, idrefbr
  ;
  ; ritardi delle riflessioni
  ;
  kdrefdelll = kdrefll / gispeed
  kdrefdellr = kdreflr / gispeed
  kdrefdelrl = kdrefrl / gispeed
  kdrefdelrr = kdrefrr / gispeed
  kdrefdelfl = kdreffl / gispeed
  kdrefdelfr = kdreffr / gispeed
  kdrefdelbl = kdrefbl / gispeed
  kdrefdelbr = kdrefbr / gispeed
  ;
  ; abbattimento ampiezze delle riflessioni
  ;
  karefampll = giassorb / kdrefll
  karefamplr = giassorb / kdreflr
  karefamprl = giassorb / kdrefrl
  karefamprr = giassorb / kdrefrr
  karefampfl = giassorb / kdreffl
  karefampfr = giassorb / kdreffr
  karefampbl = giassorb / kdrefbl
  karefampbr = giassorb / kdrefbr

  ; 
  ;       2) calcolare il all pass (per fare il resto)
  ;
  ; allpass pre-delay
  ;
  iallpx = (iwcornerlx-iwcornerrx)
  iallpy = (iwcornerly-iwcornerry)
  idistallp  = sqrt((iallpx^2) + (iallpy^2))
  idelallp  = idistallp / gispeed     ; pre-delay
  iampallp  = giassorb  / idistallp   ; assorbimento


	abufl delayr 2
  ;
  ; diretto
  ;
	adirl deltap kdel
	adirr deltap kder
  ;
  ; prime riflessioni
  ;
  arefll deltap kdrefdelll
  areflr deltap kdrefdellr
  arefrl deltap kdrefdelrl
  arefrr deltap kdrefdelrr
  areffl deltap kdrefdelfl
  areffr deltap kdrefdelfr
  arefbl deltap kdrefdelbl
  arefbr deltap kdrefdelbr
  aallppd deltap idelallp   ; pre-delay del allpass
  
	delayw ain


; 
aallpout alpass aallppd, 1.8, idelallp/10
; 

  aleft  = adirl*kampl + arefll*karefampll + arefrl*karefamprl + areffl*karefampfr + arefbl*karefampbl + aallpout*iampallp
  aright = adirr*kampr + areflr*karefamplr + arefrr*karefamprr + areffr*karefampfr + arefbr*karefampbr + aallpout*iampallp

	outs aleft, aright
	
	zacl iindex, iindex
endin
