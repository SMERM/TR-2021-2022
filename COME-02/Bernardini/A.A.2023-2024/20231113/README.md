# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 13/11/2023

### Argomenti

* `pure data`: controlli multipli modulati

### Codice 

[mobile_faders.pd](./mobile_faders.pd)<break />
![[mobile_faders.pd](./mobile_faders.pd)](./mobile_faders.png)

[poc.pd](./poc.pd)<break />
![[poc.pd](./poc.pd)](./poc.png)
