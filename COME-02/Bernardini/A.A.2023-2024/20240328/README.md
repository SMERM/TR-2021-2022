# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini

## Lezione del 28/03/2024

### Argomenti

* realizzazione di un lettore di analisi lpc
  (realizzate in python in [questo repository](https://gitlab.com/SMERM/lpc_analysis/-/tree/tr2021-2022?ref_type=heads))
  in `pure data`

### Lavagne

![whiteboard 1](./whiteboard_20240328.png)

### Codice `pure data`

![pulse generator](./pulse_generator.png)

![lpc main screen (unfinished)](./lpc_main_screen_unfinished.png)
