# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 18/12/2023

### Argomenti

* lavori individuali
* discussione sul linguaggio `faust`

### Lavagne

![TR_III_18-12-2023_15.39_1.jpg](./TR_III_18-12-2023_15.39_1.jpg)

![TR_III_18-12-2023_15.39_2.jpg](./TR_III_18-12-2023_15.39_2.jpg)
